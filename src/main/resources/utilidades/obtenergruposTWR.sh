#!/bin/bash

OLDIFS=$IFS
IFS=\;


cat gruposACC.txt | awk -F "\n" '

BEGIN {
	C1 = 1
	FICHEROC1="gruposConfR.txt"
	BLOQUE=""
	print > FICHEROC1
}

/Interface/ {
  BLOQUE = ""
}

/Interface R1/ {
  BLOQUE = "C1"
} 

/./ {
  if ($0 ~ /224\.10\..*/) {
      gsub (/[0-9]+[wdm].+/, "", $1)
      gsub (/ +/, "", $1)
    
      if (BLOQUE == "C1") {
        LINEA = sprintf("R_MCAST_%d=%s;%s;1600", C1, $1, $1)
        print LINEA >> FICHEROC1
        C1 = C1 + 1;
      } 
    }
}'

IFS=$OLDIFS
