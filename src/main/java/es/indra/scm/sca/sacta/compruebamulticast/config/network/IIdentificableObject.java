/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

/**
 *
 * @author aferrero
 */
public interface IIdentificableObject {

    void setId(String id);
    String getId();
}
