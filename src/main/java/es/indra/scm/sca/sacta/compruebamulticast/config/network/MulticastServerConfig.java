/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.net.InetAddress;

/**
 *
 * @author aferrero
 */
public class MulticastServerConfig {

    private InetAddress addr;
    private String multicastIP;
    private int port;
    private int lan;
    
    public MulticastServerConfig(InetAddress addr, String multicastIP, int port, int lan) {
        this.addr = addr;
        this.multicastIP = multicastIP;
        this.port = port;
        this.lan = lan;
    }

    @Override
    public String toString() {
        return "addr: " + getAddr() + " multicastIP: " + getMulticastIP() + " port: " + getPort();
    }

    /**
     * @return the addr
     */
    public InetAddress getAddr() {
        return addr;
    }

    /**
     * @return the multicastIP
     */
    public String getMulticastIP() {
        return multicastIP;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }
    
    public int getLan() {
        return lan;
    }
}
