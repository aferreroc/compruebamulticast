/**
 * *****************************************************************************
 * Name : es.indra.scm.sacta.config.LegacyNetworkDomainConfigReader
 *
 * Project : [SC|SGU|PSI|...] Subproject: spv-alm CSCI : CSC :
 * ****************************************************************************** Last change info.
 * ****************************************************************************** #$Author: jrquinones $ #$Date: 2011-11-24 12:09:55 +0100
 * (jue, 24 nov 2011) $ #$Revision: 55067 $ ******************************************************************************
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import es.indra.scm.sca.sacta.compruebamulticast.utils.IOUtils;
import es.indra.scm.sca.sacta.compruebamulticast.utils.StringUtils;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Class
 * <code>LegacyNetworkDomainConfigReader</code>. It has labels and methods for network configuration.
 */
public class LegacyNetworkDomainConfigReader {

    private static final Logger log = Logger.getLogger(LegacyNetworkDomainConfigReader.class.getName());
    /**
     * Start for the DOMAIN block.
     */
    public static final String INIT_DOMAIN_BLOCK = "DOMINIO";
    /**
     * End for the DOMAIN block.
     */
    public static final String END_DOMAIN_BLOCK = "FIN_DOMINIO";
    /**
     * Start for the CENTRE block.
     */
    public static final String INIT_CENTRE_BLOCK = "CENTRO";
    /**
     * End for the CENTRE block.
     */
    public static final String END_CENTRE_BLOCK = "FIN_CENTRO";
    /**
     * Attribute to read ID.
     */
    public static final String CENTRE_ID_ATTR = "ID";
    /**
     * Attribute to read IP.
     */
    public static final String CENTRE_IP_ATTR = "IP";
    /**
     * Attribute to read MASK.
     */
    public static final String CENTRE_MASK_ATTR = "MASK";
    /**
     * Attribute to read GATEWAY.
     */
    public static final String CENTRE_GATEWAY_ATTR = "GATEWAY";
    /**
     * Attribute to read X25LINK.
     */
    public static final String CENTRE_X25LINK_ATTR = "ENLACE_X25";
    /**
     * List of mapPuertos.
     */
    private HashMap<String, List<String>> mapPuertos = new HashMap<String, List<String>>();
    /**
     * <
     * code>NetworkDomainConfigContext</code> NetworkDomainConfigContext.
     */
    private NetworkDomainConfigContext networkDomainConfigContext;
    /**
     * List of control.
     */
    private LinkedList<Object> controlList = new LinkedList<Object>();
    /**
     * String Regular Expression Octet.
     */
    private final String octRegex = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    /**
     * String Regular Expression IP.
     */
    private final String ipRegex = "^(?:" + octRegex + "\\.){3}" + octRegex + "$";
    /**
     * String Regular Expression Node.
     */
    private final String nodeIPRegex = "0\\." + octRegex;
    /**
     * String Regular Expression Group.
     */
    private final String groupIPRegex = octRegex + "\\." + octRegex;
    /**
     * String Regular Expression Separation.
     */
    private final String sepRegex = "(\\s+|\\t+){1,}";
    /**
     * IP Pattern.
     */
    private final Pattern ipPattern = Pattern.compile(ipRegex);
    /**
     * Node IP Part Pattern.
     */
    private final Pattern nodeIPPartPattern = Pattern.compile(nodeIPRegex);
    /**
     * Group IP Part Pattern.
     */
    private final Pattern groupIPPartPattern = Pattern.compile(groupIPRegex);
    /**
     * Route Pattern.
     */
    private final Pattern routePattern = Pattern.compile(".{1,}" + sepRegex + "->" + sepRegex + ".{1,}");

    /**
     * Parses the file for reading configuration.
     *
     * @param networkDomainConfigContext Network Domain Config Context
     * @param file File
     * @throws Exception General Exception.
     */
    public void parse(NetworkDomainConfigContext networkDomainConfigContext, File file) throws Exception {
        try {
            parse(networkDomainConfigContext, new FileInputStream(file));
        } catch (FileNotFoundException e) {
            log.log(Level.SEVERE, "Exception Ocurred.", e);
        }
    }

    /**
     * Parse the resources for the network configuration.
     *
     * @param networkDomainConfigContext 
     * @param resource Resource.
     * @throws Exception General Exception.
     */
    public void parse(NetworkDomainConfigContext networkDomainConfigContext, String resource) throws FileNotFoundException {
        parse(networkDomainConfigContext, IOUtils.getInputStream(resource));
    }

    /**
     * Parse the Input Stream.
     *
     * @param networkDomainConfigContext Network Domain Config Context
     * @param is Input Stream.
     * @throws Exception General Exception.
     */
    public void parse(NetworkDomainConfigContext networkDomainConfigContext, InputStream is)  {

        this.networkDomainConfigContext = networkDomainConfigContext;

        try {
            InputStreamReader isr = new InputStreamReader(is);

            BufferedReader in = new BufferedReader(isr);

            String str;

            BlockProcessor currentBlockProcessor = null;

            BlockProcessor[] blockProcessors = {new DomainBlockProcessor(), new CentreBlockProcessor(),
                new RouteBlockProcessor(), new NodeBlockProcessor()};

            while ((str = in.readLine()) != null) {


                str = str.trim();

                String[] items = str.split(" +|\t+");

                String key = getAttrValue(items, 0);

                //if (key != null && !key.isEmpty() && !key.startsWith("#") && !key.startsWith("*")) {
                if (key != null && key.length()!=0 && !key.startsWith("#")) {

                    boolean processed = false;

                    if (currentBlockProcessor != null) {
                        processed = currentBlockProcessor.accept(str, items);
                    }

                    if (!processed) {

                        currentBlockProcessor = null;

                        for (BlockProcessor blockProcessor : blockProcessors) {
                            processed = blockProcessor.accept(str, items);

                            if (processed) {

                                if (blockProcessor.isMultiline()) {
                                    currentBlockProcessor = blockProcessor;
                                }

                                break;
                            }
                        }

                        if (!processed) {
                            log.log(Level.SEVERE, "Line not processed [" + str + "]");
                        }
                    }
                }
            }

            in.close();

            isr.close();
        } catch (IOException e) {
            log.log(Level.SEVERE, "Parsing resource", e);
        }
    }

    /**
     * Returns node of
     * <code>INode</code>.
     *
     * @param id Identifier.
     * @return
     * <code>INode</code> Node.
     */
    protected INode getNode(String id) {
        
        INode node = networkDomainConfigContext.getNode(id);

        if (node == null) {

            node = new Node(networkDomainConfigContext, id);

            List<String> portStrList = mapPuertos.get(id);

            if (portStrList != null) {
                for (String portStr : portStrList) {
                    setPort(node, portStr);
                }
            }

            networkDomainConfigContext.addNode(node);
        }

        return node;
    }

    /**
     * Assign the port.
     *
     * @param node Node
     * @param portStr String port.
     */
    protected void setPort(INode node, String portStr) {

        //System.out.println("setPort - node " + node.getId() + " [" + portStr + "]");

        String[] portPart = (portStr != null ? portStr.split(":") : null);

        if (portPart != null && portPart.length > 1) {
            // protocolo:puerto

            int port = StringUtils.getIntegerValue(portPart[1], -1);

            if (port == -1) {
                log.log(Level.SEVERE, "Wrong configuration for port. "
                        + "Node[" + node.getId() + "] Protocol-Port[" + portStr + "]");
            } else {
                node.addPort(portPart[0], port);
            }
        } else {
            //asumimos udp
            int port = StringUtils.getIntegerValue(portStr, -1);

            if (port == -1) {
                log.log(Level.SEVERE, "Wrong configuration for port. "
                        + "Node[" + node.getId() + "] Protocol-Port[" + portStr + "]");
            } else {
                node.addPort("udp", port);
            }
        }
    }

    /**
     * Return Centre Ip Part.
     *
     * @param attrs Attributes.
     * @param index Index
     * @param isMulticast Multicast.
     * @return String Ret
     */
    protected String getCentreIPPart(String[] attrs, int index, boolean isMulticast) {
        String ret = null;

        String ip = getAttrValue(attrs, index);

        if (ip != null) {
            String[] ipParts = ip.split("\\.");

            if (ipParts != null) {
                if (ipParts.length == 2) {
                    ret = ipParts[0] + "." + ipParts[1];
                } else if (ipParts.length == 4) {

                    if (isMulticast) {
                        ret = ipParts[0] + "." + ipParts[1];
                    } else {
                        ret = ipParts[1] + "." + ipParts[2];
                    }
                } else {
                    log.log(Level.SEVERE, "Wrong definition for getCentreIPPart ip [" + ip + "]");
                }
            }
        }

        return ret;
    }

    /**
     * Return Value Attribute.
     *
     * @param attrs Attribute
     * @param index Index
     * @return String value
     */
    protected String getAttrValue(String[] attrs, int index) {

        String value = null;

        if (attrs != null && attrs.length > index && attrs[index] != null) {
            value = attrs[index].trim();
        }

        return value;
    }

    /**
     * Return NetworkDomainConfigContext.
     *
     * @return
     * <code>NetworkDomainConfigContext</code>
     */
    public NetworkDomainConfigContext getNetworkDomainConfigContext() {
        return networkDomainConfigContext;
    }

    /**
     * Class
     * <code>Block Processor</code>.
     */
    private class BlockProcessor {

        /**
         * Boolean Multiline.
         */
        private boolean multiLine = true;

        /**
         * Set variable multiline.
         *
         * @param multiLine MultiLine
         */
        protected BlockProcessor(boolean multiLine) {
            this.multiLine = multiLine;
        }

        /**
         * Method Accept.
         *
         * @param str String str
         * @param items Items.
         * @return false if don't accept
         */
        public boolean accept(String str, String[] items) {
            return false;
        }

        /**
         * Get variable multiline.
         *
         * @return multiLine
         */
        public boolean isMultiline() {
            return multiLine;
        }
    }

    /**
     * Class
     * <code>DomainBlockProcessor</code> extends
     * <code>BlockProcessor</code>.
     */
    private class DomainBlockProcessor extends BlockProcessor {

        /**
         * Default Constructor.
         */
        public DomainBlockProcessor() {
            super(true);
        }

        @Override
        public boolean accept(String str, String[] items) {

            boolean ret = false;

            if (items != null) {

                if (INIT_DOMAIN_BLOCK.equalsIgnoreCase(items[0])) {
                    Domain domain = new Domain();

                    processDomainAttributes(domain, items);

                    networkDomainConfigContext.addDomain(domain);
                    controlList.addLast(domain);

                    ret = true;
                } else if (!controlList.isEmpty() && controlList.getLast() instanceof Domain
                        && END_DOMAIN_BLOCK.equalsIgnoreCase(items[0])) {

                    controlList.removeLast();

                    ret = true;
                }
            }

            return ret;
        }

        /**
         * Process Domain Attributes Method. Is assigned to the domain name and id attributes.
         *
         * @param domain Domain
         * @param attrs Attributes
         */
        protected void processDomainAttributes(Domain domain, String[] attrs) {
            if (attrs != null) {

                domain.setId(getAttrValue(attrs, 1));
                domain.setName(getAttrValue(attrs, 2));

                String ip = getAttrValue(attrs, 3);

                if (ip != null) {
                    String[] ipParts = ip.split("\\.");

                    domain.setIPPart(getAttrValue(ipParts, 0));
                }
            }
        }
    }

    /**
     * Class
     * <code>CentreBlockProcessor</code> extends
     * <code>BlockProcessor</code>.<br>
     *
     */
    private class CentreBlockProcessor extends BlockProcessor {

        /**
         * <
         * code>Domain</code> Parent Domain.
         */
        private Domain parentDomain;
        /**
         * <
         * code>Centre</code> Current Centre.
         */
        private Centre currentCentre;

        /**
         * Default Constructor.
         */
        public CentreBlockProcessor() {
            super(true);
        }

        @Override
        public boolean accept(String str, String[] items) {

            boolean ret = false;

            if (items != null) {

                if (currentCentre == null) {
                    Object parent = (controlList.isEmpty() ? null : controlList.getLast());

                    if (parent instanceof Domain) {
                        parentDomain = (Domain) parent;
                    }

                    if (INIT_CENTRE_BLOCK.equalsIgnoreCase(items[0])) {

                        currentCentre = new Centre(parentDomain);

                        controlList.addLast(currentCentre);

                        ret = true;
                    }
                } else if (END_CENTRE_BLOCK.equalsIgnoreCase(items[0])) {

                    if (currentCentre == null) {
                        log.log(Level.SEVERE,
                                "End of centre without created centre");
                    } else if (parentDomain == null) {
                        log.log(Level.SEVERE,
                                "read - Centre without reference domain [" + currentCentre.getId() + "]");
                    } else {
                        parentDomain.add(currentCentre);
                    }

                    networkDomainConfigContext.addCentre(currentCentre);
                    controlList.removeLast();

                    parentDomain = null;
                    currentCentre = null;

                    ret = true;
                } else if (currentCentre != null) {
                    // Parametros de un centro....

                    processCentreAttributes(currentCentre, items);

                    ret = true;
                }

            }

            return ret;
        }

        /**
         * Process Centre Attributes Method. Center is assigned to the corresponding attribute.
         *
         * @param centre Centre
         * @param attrs Attributes
         */
        protected void processCentreAttributes(Centre centre, String[] attrs) {
            if (attrs != null) {
                String key = getAttrValue(attrs, 0);

                if (key != null && key.length()!=0) {
                    if (CENTRE_ID_ATTR.equalsIgnoreCase(key)) {
                        centre.setId(getAttrValue(attrs, 1));
                        centre.setName(getAttrValue(attrs, 2));
                    } else if (CENTRE_IP_ATTR.equalsIgnoreCase(key)) {

                        String ucIpPart1 = getCentreIPPart(attrs, 1, false);
                        // if (ucIpPart1 != null) {
                        // centre.addUnicastIPPart(ucIpPart1);
                        // }

                        String ucIpPart2 = getCentreIPPart(attrs, 2, false);
                        // if (ucIpPart2 != null) {
                        // centre.addUnicastIPPart(ucIpPart2);
                        // }

                        String mcIpPart1 = getCentreIPPart(attrs, 3, true);
                        // if (mcIpPart1 != null) {
                        // centre.addMulticastIPPart(mcIpPart1);
                        // }

                        String mcIpPart2 = getCentreIPPart(attrs, 4, true);
                        // if (mcIpPart2 != null) {
                        // centre.addMulticastIPPart(mcIpPart2);
                        // }

                        if (ucIpPart1 != null || mcIpPart1 != null) {
                            centre.addPairIPParts(ucIpPart1, mcIpPart1, 1);
                        }

                        if (ucIpPart2 != null || mcIpPart2 != null) {
                            centre.addPairIPParts(ucIpPart2, mcIpPart2, 2);
                        }

                    } else if (CENTRE_MASK_ATTR.equalsIgnoreCase(key)) {
                        centre.setNetworkMask(getAttrValue(attrs, 1));
                    } else if (CENTRE_GATEWAY_ATTR.equalsIgnoreCase(key)) {
                        centre.setGateway(getAttrValue(attrs, 1));
                    } else if (CENTRE_X25LINK_ATTR.equalsIgnoreCase(key)) {
                        centre.setX25Link(getAttrValue(attrs, 1));
                    }
                }
            }
        }
    }

    /**
     * Class
     * <code>RouteBlockProcessor</code> extends
     * <code>BlockProcessor</code>.
     */
    private class RouteBlockProcessor extends BlockProcessor {

        /**
         * Default Constructor.
         */
        public RouteBlockProcessor() {
            super(false);
        }

        @Override
        public boolean accept(String str, String[] items) {
            boolean ret = false;

            //System.out.println("accept - " + str);

            if (routePattern.matcher(str).matches()) {
                String[] routeParts = str.split("(\\s+|\\t+){1,}");

                //System.out.println("routeParts - " + routeParts.length);

                if (routeParts.length == 4) {
                    String portStr = routeParts[3];

                    String id = routeParts[2];

                    INode node = networkDomainConfigContext.getNode(id);

                    // Si el nodo no existe, se guarda el puerto para
                    // despues....
                    if (node != null) {
                        setPort(node, portStr);
                    } else {
                        List<String> portStrList = mapPuertos.get(id);

                        if (portStrList == null) {
                            portStrList = new ArrayList<String>();

                            mapPuertos.put(id, portStrList);
                        }

                        portStrList.add(portStr);
                    }
                }

                ret = true;
            }

            return ret;
        }
    }

    /**
     * Class
     * <code>NodeBlockProcessor</code> extends
     * <code>BlockProcessor</code>.
     */
    private class NodeBlockProcessor extends BlockProcessor {

        /**
         * <
         * code>INode</code> lasted group.
         */
        private INode latestGroup = null;
        /**
         * Boolean next Should Be Group.
         */
        private boolean nextShouldBeGroup = false;

        /**
         * Default Constructor.
         */
        public NodeBlockProcessor() {
            super(false);
        }

        @Override
        public boolean accept(String str, String[] items) {
            boolean ret = false;

            if (str.contains(",")) {
                // it is a group where there is no whitespace between commas and
                // values

                if (nextShouldBeGroup || str.startsWith(",")) {
                    // continuo un grupo previo...

                    processMulticastGroup(latestGroup, str, items);

                    ret = true;
                } else if (items.length > 1) {
                    // el primer item debe ser el grupo...

                    latestGroup = getNode(items[0]);

                    processMulticastGroup(latestGroup, str, items);

                    ret = true;
                } else {
                    nextShouldBeGroup = false;

                    System.out.println("otro [" + str + "]");
                }
            } else {

                nextShouldBeGroup = false;

                int type = -1;

                String key = getAttrValue(items, 0);

                for (int i = 1; i < items.length; i++) {

                    String value = getAttrValue(items, i);

                    if (value.length()!=0) {

                        // Analizamos el tipo de la peticion....
                        if (type == -1) {

                            if (nodeIPPartPattern.matcher(value).matches()) {
                                // Es ip de nodo...
                                type = 0;
                            } else if (groupIPPartPattern.matcher(value).matches()) {
                                // Es ip de grupo
                                type = 1;
                            } else if (ipPattern.matcher(value).matches()) {
                                // Es un grupo de ip completa
                                type = 2;
                            } else {
                                type = 3;
                            }
                        }
                        
//                        System.out.println("Tipo: "+type+" [" + str + "] value[" + value + "] i="+i);

                        switch (type) {
                            case 0:
                                ret = true;
//                                 System.out.println("nodo - ip[" + str + "]");

                                String[] octets = value.split("\\.");

                                if (octets.length == 2) {
                                    getNode(key).setIPPart(octets[1]);
                                }

                                break;
                            case 1:
                                ret = true;
                                
                                //System.out.println("grupo - ip[" + str + "] value[" + value + "]");
                                
                                if (groupIPPartPattern.matcher(value).matches()) {
                                    //System.out.println("grupo - ip[" + str + "] value[" + value + "]");
                                    getNode(key).setIPPart(value);
                                }

                                latestGroup = null;

                                break;
                            case 2:
                                ret = true;

                                getNode(key).addStaticIP(value, i);

                                latestGroup = null;

                                break;
                            case 3:
                                ret = true;
                                latestGroup = getNode(key);

                                processMulticastGroup(latestGroup, str, items);

                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return ret;
        }

        /**
         * It processes the multicast group.
         *
         * @param parent Parent
         * @param str String str
         * @param attrs Attributes
         */
        protected void processMulticastGroup(INode parent, String str, String[] attrs) {

            nextShouldBeGroup = false;

//            System.out.println("grupo - grupos[" + parent.getId() + "][" + str + "]");

            if (attrs != null) {

                for (String attr : attrs) {
                    String[] childrenItems = attr.split(",");

                    if (childrenItems != null) {
                        for (String childItem : childrenItems) {

                            childItem = childItem.trim();

                            if (childItem.length()!=0 && !parent.getId().equalsIgnoreCase(childItem)) {

                                INode node = getNode(childItem);
                                node.addParent(parent);

                                parent.addChild(node);
                            }
                        }
                    }
                }
            }

            nextShouldBeGroup = str.endsWith(",");
        }
    }
}
