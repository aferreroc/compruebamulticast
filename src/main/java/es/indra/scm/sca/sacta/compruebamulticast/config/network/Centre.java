/*******************************************************************************
 * Name      : es.indra.scm.sacta.config.Centre
 * Project   : [SC|SGU|PSI|...]
 * Subproject: spv-alm
 * CSCI      :
 * CSC       :
 *******************************************************************************
 * Last change info.
 *******************************************************************************
 * #$Author: dmoya $
 * #$Date: 2011-09-16 11:59:51 +0200 (vie, 16 sep 2011) $
 * #$Revision: 51341 $
 *******************************************************************************
 */

package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;



/**
 * Class <code>Centre</code> implements <code>ICentre</code>.
 * Defines the parameters of the center.
 */

public class Centre extends IdentificableObject implements ICentre {

	/** */
	private static final long serialVersionUID = 1L;

	/** String Name.	 */
	private String name;

	/** String Gateway. */
	private String gateway;

	/** String Network Mask. */
	private String networkMask;

	/** String x25Link. */
	private String x25Link;

	/** Domain type <code>IDomain</code>. */
	private IDomain domain;

	//	private ArrayList<String> unicastIPPart;
	//	private ArrayList<String> multicastIPPart;

	/** Id Parts Pairs List.*/
	private ArrayList<IPPartsPair> ipPartsPairs;

	/** Default Constructor. */
	public Centre() { }

	/**
	 * Constructor that receives the domain as parameter.
	 * @param domain Domain.
	 */
	public Centre(IDomain domain) {
		this.domain = domain;
	}


	public void setId(String id) {
		super.setId(id);
	}

	
	public String getName() {
		return name;
	}

	
	public void setName(String name) {
		this.name = name;
	}

	
	public String getGateway() {
		return gateway;
	}

	/**
	 * Assign a gateway to a center.
	 * @param gateway String Gateway.
	 */
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	
	public String getNetworkMask() {
		return networkMask;
	}

	/**
	 * Assign a Network Mask to a center.
	 * @param networkMask String Network Mask
	 */
	public void setNetworkMask(String networkMask) {
		this.networkMask = networkMask;
	}


	
	public String getX25Link() {
		return x25Link;
	}

	/**
	 * Assign a x25Link to a center.
	 * @param x25Link String x25Link
	 */
	public void setX25Link(String x25Link) {
		this.x25Link = x25Link;
	}


	
	public IDomain getDomain() {
		return domain;
	}


	//	public void addMulticastIPPart(String ipPart) {
	//		if (multicastIPPart == null) {
	//			multicastIPPart = new ArrayList<String>();
	//		}
	//
	//		multicastIPPart.add(ipPart);
	//	}
	//
	//	public void addUnicastIPPart(String ipPart) {
	//		if (unicastIPPart == null) {
	//			unicastIPPart = new ArrayList<String>();
	//		}
	//
	//		unicastIPPart.add(ipPart);
	//	}


	/**
	 * Add a PairIPParts to the center.
	 *
	 * @param unicastIPPart Unicast IP Part.
	 * @param multicastIPPart Multicast IP Part.
	 *
	 */
	public void addPairIPParts(String unicastIPPart, String multicastIPPart, int lan) {
		addPairIPParts(new IPPartsPair(unicastIPPart, multicastIPPart, lan));
	}

	/**
	 *
	 * Add a PairIPParts to the center.
	 * @param ipPartsPair ipPartsPair
	 */
	public void addPairIPParts(IPPartsPair ipPartsPair) {
		if (ipPartsPairs == null) {
			ipPartsPairs = new ArrayList<IPPartsPair>();
		}

		ipPartsPairs.add(ipPartsPair);
	}


	/**
	 * Get a list of IP Parts Pairs.
	 *
	 * @return ipPartsPairs
	 */
   
	public List<IPPartsPair> getIPPartsPairs() {
		return ipPartsPairs;
	}

	
	public boolean isUnicastIPPart(String ipPart) {
		boolean ret = false;

		if (ipPart != null) {
			for (IPPartsPair ipPartPair : ipPartsPairs) {
				if (ipPart.equals(ipPartPair.getUnicastIPPart())) {
					ret = true;
					break;
				}
			}
		}

		return ret;
	}


	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public List<String> getMulticastIPParts() {
	//		return (multicastIPPart == null ? EmptyList.getInstance() : multicastIPPart);
	//	}
	//
	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public List<String> getUnicastIPParts() {
	//		return (unicastIPPart == null ? EmptyList.getInstance() : unicastIPPart);
	//	}


	
	public void toXML(PrintWriter out) {
		out.println("<centre " + XMLATTR_ID + "=\"" + getId() + "\" name=\"" + name + "\">");
		//		for (String ipPart : getUnicastIPParts()) {
		//			out.println("\t<unicastIpPart id=\"" + ipPart + "\"/>");
		//		}
		//		for (String ipPart : getMulticastIPParts()) {
		//			out.println("\t<multicastIpPart id=\"" + ipPart + "\"/>");
		//		}
		out.println("</centre>");
	}

    
}
