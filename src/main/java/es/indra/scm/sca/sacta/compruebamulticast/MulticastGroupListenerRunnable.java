/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author aferrero
 */
public class MulticastGroupListenerRunnable implements Runnable {

    private static final Logger log = Logger.getLogger(MulticastGroupListenerRunnable.class.getName());
    private BlockingQueue<Secuencia> queue;
    private boolean isLan1;
    private MulticastSocket mcastSocket;
    private SocketAddress groupSockAddr;
    private NetworkInterface networkInterface;
    private boolean isRunning = true;
    //private int sockTimeout = 5 * 1000;

    public MulticastGroupListenerRunnable(
            String interfazRed, String mcast_group,
            int mcast_port, BlockingQueue<Secuencia> queue, boolean isLan1) throws IOException {


        this.queue = queue;
        this.isLan1 = isLan1;

        log.info("Suscribiendo... " + interfazRed + " " + mcast_group + " " + mcast_port);


        InetAddress iAddr_mcast_group = InetAddress.getByName(mcast_group);
        groupSockAddr = new InetSocketAddress(iAddr_mcast_group, mcast_port);

        networkInterface = NetworkInterface.getByName(interfazRed);


        //mcastSocket = new MulticastSocket(mcast_port);
        mcastSocket = new MulticastSocket(groupSockAddr);
        mcastSocket.setNetworkInterface(networkInterface);
        //mcastSocket.setSoTimeout(sockTimeout); 

        mcastSocket.joinGroup(groupSockAddr, networkInterface);
        log.info("Suscrito! " + mcastSocket.getInterface() + " " + groupSockAddr.toString());

    }

    public void start() {
        isRunning = true;
        try {
            mcastSocket.joinGroup(groupSockAddr, null);
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Para parar el thread
     */
    public void stop() {
        isRunning = false;
        try {
            mcastSocket.leaveGroup(groupSockAddr, null);
            //mcastSocket.close();
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
        }


        queue.clear();

    }

    public void run() {

        isRunning = true;

        byte[] buffer = new byte[20]; //la cabecera sacta tiene 20 bytes
        DatagramPacket data = new DatagramPacket(buffer, buffer.length);


        try {
            while (true) {

                if (!isRunning) {
                    Thread.sleep(2000);
                }


                mcastSocket.receive(data);
                
                log.log(Level.FINE, "UDP: " + new String(Hex.encodeHex(buffer))); 
                
                //secuencia = dameNumeroSecuencia(buffer, data.getLength());

                MensajeSACTA mensajeSacta = new MensajeSACTA(buffer);     
                log.log(Level.FINE, "Cabecera SACTA: " + mensajeSacta.toString()+" Secuencia (int): " + mensajeSacta.getSequenceIntValue());

//                log.log(Level.FINE, "Lan " + (isLan1 ? "1: " : "2: ") + secuencia 
//                        + " Host origen: " + data.getAddress().getHostAddress() 
//                        + " interface: " + mcastSocket.getInterface().getHostAddress()
//                        +" grupo mcast: " + groupSockAddr.toString());


                queue.put(new Secuencia(mensajeSacta.getSequenceIntValue(), isLan1, data.getAddress().getHostAddress()));

            }
        } catch (InterruptedException ex) {
            log.log(Level.INFO, null, ex);
        } catch (IOException ex) {
            log.info("Excepcion: " + ex.getMessage());
        } finally {
            try {
                mcastSocket.leaveGroup(groupSockAddr, null);
            } catch (Exception exx) {
            }
            try {
                mcastSocket.close();
            } catch (Exception exx) {
            }
        }

    }

    public static int dameNumeroSecuencia(byte[] buffer, int lenDatos) {
        //una cabecera sacta tiene 160 bits == 20 bytes
        //la secuencia empieza en el bit 100 y mide 12 bits

        //por lo que nos interesan los bytes 13 y 14 (empezando en 0 son los bytes 12 y 13)

//        for (int i = 0; i < lenDatos; i++) {
//            System.out.println("Lan " + (isLan1 ? "1" : "2") + "-> Byte " + i + ": " + Integer.toBinaryString(buffer[i] & 0xFF));
//        }


        String strSecuencia = "";;
        String byte12 = "";
        String byte13 = "";
        try {

            if (lenDatos >= 14) {
                byte aByte12 = buffer[12];
                byte aByte13 = buffer[13];


                byte12 = Integer.toBinaryString((aByte12 >> 4) & 0xFF); // nos interesan los ultimos 4 bits, asi que desplazamos
                byte13 = Integer.toBinaryString(aByte13 & 0xFF);

                strSecuencia = byte12 + byte13;
                //System.out.println("Secuencia: " + strSecuencia + " netif: " + networkInterface);

                int intSecuencia = Integer.valueOf(strSecuencia, 2);
                //System.out.println("valor secuencia: " + intSecuencia + " netif: " + networkInterface);

                return intSecuencia;
            }

            return Secuencia.NO_NUMERO;

        } catch (NumberFormatException ex) {
            log.info("Error al intentar convertir en numero byte12->" + byte12 + " byte13->" + byte13 + " secuencia: " + strSecuencia);
            return Secuencia.NO_NUMERO;
        }
    }

    
}
