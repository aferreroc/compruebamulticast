/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import es.indra.scm.sca.sacta.compruebamulticast.ParGrupoMulticast;
import es.indra.scm.sca.sacta.compruebamulticast.config.Configuracion;
import es.indra.scm.sca.sacta.compruebamulticast.config.ConfiguracionException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author aferrero
 */
public class SACTAEnvironment {

    private static final Logger log = Logger.getLogger(SACTAEnvironment.class.getName());
    //
    private static SACTAEnvironment _instance;
    //
    private String sactaPath = null;
    //
    private String NODO = null;
    //UCS:
//    CENTRO=ACC_LECM
//    CENTRO_LAN2=ACC_LECM_R
    //POS
//    CENTRO=ACC_LECM_R
//    CENTRO_LAN2=RED_FOCUS
    private String CENTRO = null;
    private String CENTRO_LAN2 = null;
    private String DOMINIO = null;
    private String ficheroCENTRO = null;
    private String ficheroCENTROLAN2 = null;
    private NetworkDomainConfigContext networkDomainConfigContextCentro = null, networkDomainConfigContextCentroLan2 = null;

    public static void main(String args[]) throws IOException, ConfiguracionException {
        if (args.length < 1) {
            SACTAEnvironment.getInstance(
                    "/Users/aferrero/Developer/proyectos/INDRA/CompruebaMulticast/src/main/resources/ficheros_sacta");    
        } else {
            SACTAEnvironment.getInstance(args[0]);
        }
        doTest();
    }
    
    public static void doTest() {

//        System.out.println(SACTAAgentNamingUtils.convertLabelIdToComIPCentrosId("POS_TMA_B_6".toLowerCase()));
        
        System.out.println("Etiqueta (NODO): " + SACTAEnvironment.getInstance().getMyNodeId());
        System.out.println("CENTRO: " + SACTAEnvironment.getInstance().getMyCentreId());
        System.out.println("CENTRO_LAN2: " + SACTAEnvironment.getInstance().getMyCentreIdLan2());
        System.out.println("DOMINIO: " + SACTAEnvironment.getInstance().getMyDomainId());
        System.out.println("ficheroCENTRO: " + SACTAEnvironment.getInstance().ficheroCENTRO);
        System.out.println("ficheroCENTROLAN2: " + SACTAEnvironment.getInstance().ficheroCENTROLAN2);



        for (MulticastServerConfig server :
                NetworkDomainUtil.getMulticastConfig(
                SACTAEnvironment.getInstance().getNetworkDomainConfigContext(),
                SACTAEnvironment.getInstance().getMyDomainId(),
                SACTAEnvironment.getInstance().getMyCentreId(),
                SACTAEnvironment.getInstance().getMyNodeId(),
                "udp")) {
            System.out.println("CENTRO: " + server.toString());
        }

        for (MulticastServerConfig server :
                NetworkDomainUtil.getMulticastConfig(
                SACTAEnvironment.getInstance().getNetworkDomainConfigContextLan2(),
                SACTAEnvironment.getInstance().getMyDomainId(),
                SACTAEnvironment.getInstance().getMyCentreIdLan2(),
                SACTAEnvironment.getInstance().getMyNodeId(),
                "udp")) {
            System.out.println("CENTRO_LAN2: " + server.toString());
        }



//        List<ParGrupoMulticast> listaControl = interpretarFichero(ficheroCONTROL, domainId, CENTRO);
//        List<ParGrupoMulticast> listaRadar = interpretarFichero(ficheroRADAR, domainId, CENTRO_LAN2);
    }

    private SACTAEnvironment() {

        sactaPath = Configuracion.SACTA_PATH;
        if (sactaPath == null) {
            sactaPath = "/sacta/version/ejecucion/";
        }
        if (!sactaPath.endsWith(File.separator)) {
            sactaPath += File.separator;
        }

        leerEtiqueta();
        leerEntorno();
        //networkDomainConfigContext = new NetworkDomainConfigContext(SACTA_PATH + ficheroCONTROL);
        try {
            networkDomainConfigContextCentro = new NetworkDomainConfigContext(sactaPath + ficheroCENTRO);
        } catch (FileNotFoundException ex) {
            log.info("No se ha podido leer el fichero: " + ex.getMessage());
        }
        try {
            networkDomainConfigContextCentroLan2 = new NetworkDomainConfigContext(sactaPath + ficheroCENTROLAN2);
        } catch (FileNotFoundException ex) {
            log.info("No se ha podido leer el fichero: " + ex.getMessage());
        }
    }

    private SACTAEnvironment(String strSactaPath) {

        sactaPath = strSactaPath;

        if (!sactaPath.endsWith(File.separator)) {
            sactaPath += File.separator;
        }

        leerEtiqueta();

        leerEntorno();
        //networkDomainConfigContext = new NetworkDomainConfigContext(SACTA_PATH + ficheroCONTROL);
        try {
            networkDomainConfigContextCentro = new NetworkDomainConfigContext(sactaPath + ficheroCENTRO);
        } catch (FileNotFoundException ex) {
            log.info("No se ha podido leer el fichero: " + ex.getMessage());
        }
        try {
            networkDomainConfigContextCentroLan2 = new NetworkDomainConfigContext(sactaPath + ficheroCENTROLAN2);
        } catch (FileNotFoundException ex) {
            log.info("No se ha podido leer el fichero: " + ex.getMessage());
        }

    }

    public static synchronized SACTAEnvironment getInstance() {
        if (_instance == null) {
            _instance = new SACTAEnvironment();
        }
        return _instance;
    }

    public static synchronized SACTAEnvironment getInstance(String sactaPath) {
        if (_instance == null) {
            _instance = new SACTAEnvironment(sactaPath);
        }
        return _instance;
    }

    public String getMyNodeId() {
        return NODO;
    }

    public String getMyCentreId() {
        return CENTRO;
    }

    public String getMyCentreIdLan2() {
        return CENTRO_LAN2;
    }

    public String getMyCentreIdComplementary() {
        return null;
    }

    public String getMyDomainId() {
        return DOMINIO;
    }

    public NetworkDomainConfigContext getNetworkDomainConfigContext() {
        return networkDomainConfigContextCentro;
    }

    public NetworkDomainConfigContext getNetworkDomainConfigContextLan2() {
        return networkDomainConfigContextCentroLan2;
    }

    private void leerEtiqueta() {
        if (NODO == null) {
            BufferedReader br = null;

            try {
                br = new BufferedReader(new FileReader(sactaPath + "ETIQUETA.TXT"));

                String line = br.readLine().trim();


                while (line != null) {
                    if (line.length() != 0) {
                        NODO = parseaEtiqueta(line);
                        break;
                    }
                    line = br.readLine();


                }

            } catch (IOException ex) {
                Logger.getLogger(SACTAEnvironment.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    br.close();
                } catch (IOException ex) {
                    //
                }
            }
        }

    }

    public static String parseaEtiqueta(String label) {
        StringTokenizer strTok = new StringTokenizer(label, "_");
        int i = strTok.countTokens();
        if (i > 0) {
            StringBuilder strBuilder = new StringBuilder();

            while (strTok.hasMoreTokens()) {
                if (i > 2) {
                    strBuilder.append(strTok.nextToken()).append("_");
                    i--;
                } else {
                    strBuilder.append(strTok.nextToken());
                }
            }
            return strBuilder.toString();
        } else {
            return label;
        }
    }

    private void leerEntorno() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(sactaPath + "fs_gen_def_entorno.local"));

            DOMINIO = properties.getProperty("DOMINIO");

            CENTRO = properties.getProperty("CENTRO");
            ficheroCENTRO = "COM_IP_CENTROS.CFG";


//            if (CENTRO != null && CENTRO.trim().toUpperCase().endsWith("_R")) {
//                ficheroCENTRO = "COM_IP_CENTROS.CFG_radar";
//            } else if (CENTRO != null && CENTRO.trim().toUpperCase().endsWith("_FOCUS")) {
//                ficheroCENTRO = "COM_IP_CENTROS.CFG_focus";
//            } else {
//                ficheroCENTRO = "COM_IP_CENTROS.CFG_sacta";
//            }

            CENTRO_LAN2 = properties.getProperty("CENTRO_LAN2");

            if (CENTRO_LAN2 != null && CENTRO_LAN2.trim().toUpperCase().endsWith("_R")) {
                ficheroCENTROLAN2 = "COM_IP_CENTROS.CFG_radar";
            } else if (CENTRO_LAN2 != null && CENTRO_LAN2.trim().toUpperCase().endsWith("_FOCUS")) {
                ficheroCENTROLAN2 = "COM_IP_CENTROS.CFG_focus";
            } else {
                ficheroCENTROLAN2 = "COM_IP_CENTROS.CFG_sacta";
            }

        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    //
    //
    //
    //
    //
    //
    //
    public List<ParGrupoMulticast> interpretarFichero(String fichero, String dominio, String centro) throws IOException, ConfiguracionException {
        List<ParGrupoMulticast> listaDevolver = new ArrayList<ParGrupoMulticast>();


        //DOMINIO 20 MOCK_UP_5 10.0.0.0 
        Matcher matcherDominio = Pattern.compile("DOMINIO\\s+\\d+\\s+" + dominio).matcher("");
        Matcher matcherFinDominio = Pattern.compile("FIN_DOMINIO").matcher("");
        boolean tenemosDominio = false;

        //ID      200 RED_FOCUS
        Matcher matcherCentro = Pattern.compile("ID\\s+\\d+\\s+" + centro).matcher("");
        boolean tenemosCentro = false;

        //IP      0.1.50.0  0.0.0.0    224.1.0.0  0.0.0.0
        String pattIp = "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}";
        Matcher matcherIP = Pattern.compile("IP\\s+" + pattIp + "\\s+" + pattIp + "\\s+(" + pattIp + ")\\s+(" + pattIp + ")").matcher("");

        LineNumberReader lineReader = null;
        try {
            lineReader = new LineNumberReader(new FileReader(sactaPath + fichero));

            String line;
            while ((line = lineReader.readLine()) != null) {
                if (!tenemosDominio) {
                    matcherDominio.reset(line); //reset the input
                    if (matcherDominio.find()) {
                        tenemosDominio = true;
                        //System.out.println(line);
                    }
                } else if (!tenemosCentro) {
                    matcherFinDominio.reset(line);
                    if (matcherFinDominio.find()) {
                        tenemosDominio = false;
                        //System.out.println(line);
                        //break;
                        throw new ConfiguracionException("No se ha encontrado el DOMINIO en el fichero de configuracion");
                    } else {
                        matcherCentro.reset(line);
                        if (matcherCentro.find()) {
                            tenemosCentro = true;
                            //System.out.println(line);

                            //leemos siguente linea
                            line = lineReader.readLine();
                            if (line == null) {
                                break;
                            }
                            matcherIP.reset(line);
                            if (matcherIP.find()) {
                                //System.out.println(line);
                                //System.out.println(matcherIP.group(1) + " " + matcherIP.group(2));
                            }

                        }
                    }
                }

            }
        } finally {
            try {
                if (lineReader != null) {
                    lineReader.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return listaDevolver;
    }
    
    public String getFicheroCentroLan1() {
        return sactaPath + ficheroCENTRO;
    }
    
    public String getFicheroCentroLan2() {
        return sactaPath + ficheroCENTROLAN2;
    }
}
