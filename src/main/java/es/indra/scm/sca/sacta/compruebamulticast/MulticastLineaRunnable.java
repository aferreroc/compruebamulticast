/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import es.indra.scm.sca.sacta.compruebamulticast.config.Configuracion;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aferrero
 */
public class MulticastLineaRunnable implements Runnable {

    private static final Logger log = Logger.getLogger(MulticastLineaRunnable.class.getName());
    private ExecutorService executorService;
    //
    private String ifaceGR1;
    private String ifaceGR2;
    private Collection<ParGrupoMulticast> grupos;
    private McastTableModel tableModel;
    //mis Runnables
    private List<MulticastGroupListenerRunnable> listaProductores = null;
    private List<MulticastComparadorRunnable> listaConsumer = null;
    private int tiempoRefrescoColaSecs;
    private long difTiempoSecuenciasMills;

    public MulticastLineaRunnable(String ifaceGR1, String ifaceGR2, Collection<ParGrupoMulticast> grupos,
            McastTableModel tableModel, int tiempoRefrescoColaSecs, long difTiempoSecuenciasMills) {

        if (ifaceGR1 == null || ifaceGR2 == null || grupos.isEmpty()) {
            return;
        }

        this.tiempoRefrescoColaSecs = tiempoRefrescoColaSecs;
        this.difTiempoSecuenciasMills = difTiempoSecuenciasMills;


        listaConsumer = new ArrayList<MulticastComparadorRunnable>();
        listaProductores = new ArrayList<MulticastGroupListenerRunnable>();

        // 3 threads Por grupoMcast
        // 2 para  escuchar los GruposMcast
        // 1 para realizar la comparcion
        // 3 * tamano lista gruposMcast

        if (!grupos.isEmpty()) {
            this.executorService = Executors.newFixedThreadPool(3 * grupos.size());
        }


        this.ifaceGR1 = ifaceGR1;
        this.ifaceGR2 = ifaceGR2;
        this.grupos = grupos;
        this.tableModel = tableModel;
    }

    public void start() {

        for (MulticastGroupListenerRunnable productores : listaProductores) {
            productores.start();
        }

        for (MulticastComparadorRunnable consumer : listaConsumer) {
            consumer.start();
        }

    }

    public void stop() {


        for (MulticastComparadorRunnable consumer : listaConsumer) {
            consumer.stop();
        }

        //Thread.sleep(5000);


        for (MulticastGroupListenerRunnable productores : listaProductores) {
            productores.stop();
        }



//        try {
//            executorService.awaitTermination(4, TimeUnit.SECONDS);
//        } catch (InterruptedException ex) {
//            log.log(Level.INFO, null, ex);
//        }
//
//        executorService.shutdownNow();
//
//        
//        listaConsumer.clear();
//        listaProductores.clear();
//        
//        
//        if (grupos.isEmpty()) {
//            this.executorService = Executors.newFixedThreadPool(1);
//        } else {
//            this.executorService = Executors.newFixedThreadPool(3 * grupos.size());
//        }

    }

    
    public void run() {
        if (grupos==null || grupos.isEmpty() || tableModel == null) {
            return;
        }


        for (ParGrupoMulticast parGrupoMcast : grupos) {
            
            //System.out.println(parGrupoMcast.GR1);

            int row = tableModel.addMcast(parGrupoMcast);

            BlockingQueue<Secuencia> queue = new LinkedBlockingQueue<Secuencia>();


            //El comparador
            MulticastComparadorRunnable consumer = 
                    new MulticastComparadorRunnable(queue, tableModel, row, tiempoRefrescoColaSecs, difTiempoSecuenciasMills, parGrupoMcast);

            //la linea C1 o R1 
            MulticastGroupListenerRunnable producerLan1 = null;
            if (parGrupoMcast.GR1 != null && parGrupoMcast.GR1.length() != 0) {
                try {
                    producerLan1 = new MulticastGroupListenerRunnable(
                            ifaceGR1, parGrupoMcast.GR1,
                            parGrupoMcast.puerto, queue, true);
                } catch (IOException ex) {
                    tableModel.setComprobacion(Configuracion.FALLO + " en " + ifaceGR1 + " mensaje: " + ex.getMessage(), row);
                    log.log(Level.INFO, null, ex);
                }
            }



            //la linea C2 o R2
            MulticastGroupListenerRunnable producerLan2 = null;
            if (parGrupoMcast.GR2 != null && parGrupoMcast.GR2.length() != 0) {
                try {
                    producerLan2 = new MulticastGroupListenerRunnable(
                            ifaceGR2, parGrupoMcast.GR2,
                            parGrupoMcast.puerto, queue, false);
                } catch (IOException ex) {
                    tableModel.setComprobacion(Configuracion.FALLO + " en " + ifaceGR2 + " mensaje: " + ex.getMessage(), row);
                    log.log(Level.INFO, null, ex);
                }
            }



            if (producerLan1 != null && producerLan2 != null) {
                executorService.execute(producerLan1);
                executorService.execute(producerLan2);

                listaProductores.add(producerLan1);
                listaProductores.add(producerLan2);

                //El consumidor lo ejecutamos mas tarde para que se vaya cargando la queue
                //este temporizador se realiza dentro del consumer
                executorService.execute(consumer);

                listaConsumer.add(consumer);

            }


        }

    }
    
    /**
     * 
     */
    public void limpiaContadores() {
        for (MulticastComparadorRunnable mcastComparador : listaConsumer) {
            mcastComparador.limpiaContadores();
        }
    }
}
