/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

/**
 *
 * @author aferrero
 */
public class Secuencia {
    
        public static final int NO_NUMERO=-1;
        public static final int ERROR=-2;
        public static final int INTERRUMPDA=-3;

        int secuencia;
        boolean isLan1;
        String hostOrigen;

        public Secuencia(int secuencia, boolean isLan1, String hostOrigen) {
            this.secuencia = secuencia;
            this.isLan1 = isLan1;
            this.hostOrigen = hostOrigen;
        }
    }