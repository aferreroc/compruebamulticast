/**
 * *****************************************************************************
 * Name : es.indra.scm.sacta.config.NetworkDomainUtil
 *
 * Project : [SC|SGU|PSI|...] Subproject: spv-alm CSCI : CSC :
 * ****************************************************************************** Last change info.
 * ****************************************************************************** #$Author: agbarajas $ #$Date: 2011-11-29 17:02:54 +0100
 * (mar, 29 nov 2011) $ #$Revision: 55258 $ ******************************************************************************
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.io.FileNotFoundException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class
 * <code>NetworkDomainUtil</code>.
 */
public class NetworkDomainUtil {

    private static final Logger log = Logger.getLogger(NetworkDomainUtil.class.getName());

    /**
     * Gets the multicast configuration through the protocol ID.
     *
     * @param protocolId Protocol Id.
     * @return multicast configuration
     */
    public static Set<MulticastServerConfig> getMulticastConfig(String protocolId) throws FileNotFoundException {

        NetworkDomainConfigContext ndcc = SACTAEnvironment.getInstance().getNetworkDomainConfigContext();

        String domainId = SACTAEnvironment.getInstance().getMyDomainId();
        String centreId = SACTAEnvironment.getInstance().getMyCentreId();
        String agentId = SACTAEnvironment.getInstance().getMyNodeId();

        return getMulticastConfig(ndcc, domainId, centreId, agentId, protocolId);
    }

    /**
     * Gets the multicast configuration through the protocol ID and the node ID.
     *
     * @param nodeId Node ID
     * @param protocolId Protocol Id.
     * @return multicast configuration
     */
    public static Set<MulticastServerConfig> getMulticastConfig(String nodeId, String protocolId) throws FileNotFoundException {

        NetworkDomainConfigContext ndcc = SACTAEnvironment.getInstance().getNetworkDomainConfigContext();

        String domainId = SACTAEnvironment.getInstance().getMyDomainId();
        String centreId = SACTAEnvironment.getInstance().getMyCentreId();

        return getMulticastConfig(ndcc, domainId, centreId, nodeId, protocolId);
    }

    /**
     * Gets the multicast configuration.
     *
     * @param ndcc Network Domain Configuration Context
     * @param domainId Domain ID
     * @param centreId Centre ID
     * @param nodeId Node ID
     * @param protocolId Protocol ID
     * @return multicast configuration
     */
    public static Set<MulticastServerConfig> getMulticastConfig(
            NetworkDomainConfigContext ndcc,
            String domainId, String centreId, String nodeId,
            String protocolId) {
        
        if (ndcc==null) return null;

        //List<MulticastServerConfig> ret = new ArrayList<MulticastServerConfig>();
        Set<MulticastServerConfig> ret = new TreeSet<MulticastServerConfig>(new MulticastServerConfigComparator());

        IDomain domain = ndcc.getDomain(domainId);
        ICentre[] centre = new ICentre[2];
        INode node = null;

        if (domain == null) {
            log.log(Level.SEVERE, "The domain [" + domainId + "] not found in configuration file");
        } else {
            centre[0] = domain.getCentre(centreId);

            if (centre[0] == null) {
                log.log(Level.SEVERE,
                        "The centre [" + centreId + "] not found in configuration file");
            } else {
                
                node = ndcc.getNode(nodeId);

                if (node == null) {
                    log.log(Level.SEVERE,
                            "The node [" + nodeId + "] not found in configuration file");
                } else {
                    //search centre complementary
                    centre[1] = domain.getCentre(SACTAEnvironment.getInstance().getMyCentreIdComplementary());


                }

            }
        }

        if (node != null) {


            List<INode> parents = node.getParents();

            try {
                
                for (Enumeration<NetworkInterface> eIface = NetworkInterface.getNetworkInterfaces(); eIface.hasMoreElements();) {

                    NetworkInterface iface = eIface.nextElement();

                    Enumeration<InetAddress> eAddr = iface.getInetAddresses();

                    IPPartsPair partsPair = checkInterface(eAddr, centre);
                    if (partsPair != null) {


                        
                        eAddr = iface.getInetAddresses();
                        while (eAddr.hasMoreElements()) {
                            InetAddress addr = eAddr.nextElement();
                            if (addr instanceof Inet4Address) { // && !addr.isLoopbackAddress()) {


                                //System.out.println("addr: " + addr + " lan=" + lan);
                                for (INode parent : parents) {



//                                    System.out.println("addr: " + addr + " parent: " + parent.getId() + " lan=" + partsPair.getLan());
                                    ret.addAll(getMulticastConfig(ndcc, domainId, centreId, parent.getId(), protocolId));

                                    int port = parent.getPort(protocolId);

                                    if (port > 0 && parent.getIPPart() != null) {
                                        ret.add(new MulticastServerConfig(addr, partsPair.getMulticastIP(parent), port, partsPair.getLan()));
                                    }
//                                    else {
//                                        log.log(Level.WARNING,
//                                                "The group [" + parent.getId() + "] has undefine port for the protocol "
//                                                + "[" + protocolId + "]");
//                                    }

                                    //ahora las ip estaticas
                                    for (String ipMcast : parent.getMulticastStaticIP(partsPair.getLan())) {
                                        ret.add(new MulticastServerConfig(addr, ipMcast, port, partsPair.getLan()));
                                    }
                                }
                            }
                        }

                    }


//					while (eAddr.hasMoreElements()) {
//						InetAddress addr = eAddr.nextElement();
//
//						String hostAddress = addr.getHostAddress();
//
//						for (IPPartsPair ipPartPair : centre.getIPPartsPairs()) {
//
//							String unicastIP = ipPartPair.getUnicastIP(domain, node);
//
//							//if (hostAddress.equals(unicastIP)) {
//								for (INode parent : parents) {
//									int port = parent.getPort(protocolId);
//
//									if (port > 0) {
//										ret.add(new MulticastServerConfig(addr, ipPartPair.getMulticastIP(parent), port));
//									}
//									else {
//										SCLogger.error(NetworkDomainUtil.class,
//												CommunicationService.LOGGER_CATEGORY,
//												"The group [" + parent.getId() + "] has undefine port for the protocol "
//												+ "[" + protocolId + "]");
//									}
//								//}
//
//								break;
//							}
//						}
//
//						/*
//						InetAddress tmpAddr = eAddr.nextElement();
//
//						String hostAddress = tmpAddr.getHostAddress();
//
//						InetAddress addr = null;
//
//						if (ips != null && !ips.isEmpty()) {
//							for (String ip : ips) {
//								if (hostAddress.equalsIgnoreCase(ip)) {
//									addr = tmpAddr;
//									break;
//								}
//							}
//						}
//						else {
//							addr = tmpAddr;
//						}
//
//						if (addr != null) {
//							for (INode parent : parents) {
//								try {
//									int port = parent.getPort(protocolId);
//
//									// �Comprobamos el rango multicast y sacamos un mensaje?
//
//											List<String> multicastIps =
//												parent.getIPs(domainId, centreId);
//
//											for (String ip : multicastIps) {
//												ret.add(new MulticastUDPServerConfig(addr, ip, port));
//											}
//								}
//								catch (Exception e) {
//									ALMLogger.error("No hay puerto definido para [" + parent.getId()
//										+ "][" + protocolId +"] en [" + hostAddress + "]");
//								}
//							}
//						}
//						 */
//
//					}
                }
            } catch (Exception e) {
                log.log(Level.SEVERE, "", e);
            }
        }

        //Collections.sort((List<MulticastServerConfig>)ret);
        return ret;
    }

    /**
     * @param eAddr Enumeration<InetAddress>
     * @param centre ICentre[]
     * @return IPPartsPair
     */
    private static IPPartsPair checkInterface(Enumeration<InetAddress> eAddr, ICentre[] centre) {

        IPPartsPair partsPair = null;
        while (eAddr.hasMoreElements()) {
            String ipRef = eAddr.nextElement().getHostAddress();
            for (ICentre iCentre : centre) {
                if (iCentre != null) {
                    for (IPPartsPair ipPartPair : iCentre.getIPPartsPairs()) {
                        String ipPart = ipPartPair.getUnicastIPPart();
                        if (ipRef.indexOf(ipPart) > -1) {
                            return ipPartPair;
                        }
                    }
                }
            }

        }

        return partsPair;
    }

    /**
     * @param eAddr InetAddress
     * @param centre ICentre[]
     * @return boolean
     */
    public static boolean isSactaInterface(InetAddress eAddr, ICentre[] centre) {

        for (ICentre iCentre : centre) {
            if (iCentre != null) {
                for (IPPartsPair ipPartPair : iCentre.getIPPartsPairs()) {
                    String ipPart = ipPartPair.getUnicastIPPart();
                    if (eAddr.getHostAddress().indexOf(ipPart) > -1) {
                        if (eAddr instanceof Inet4Address) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Gets the server configuration through the protocol ID.
     *
     * @param protocolId Protocol Id.
     * @return server configuration
     */
    public static List<ServerConfig> getServerConfig(String protocolId) throws FileNotFoundException {

        NetworkDomainConfigContext ndcc = SACTAEnvironment.getInstance().getNetworkDomainConfigContext();

        String domainId = SACTAEnvironment.getInstance().getMyDomainId();
        String centreId = SACTAEnvironment.getInstance().getMyCentreId();
        String agentId = SACTAEnvironment.getInstance().getMyNodeId();

        return getServerConfig(ndcc, domainId, centreId, agentId, protocolId);
    }

    /**
     * Gets the server configuration through the protocol ID and the node ID.
     *
     * @param nodeId Node ID
     * @param protocolId Protocol Id.
     * @return server configuration
     */
    public static List<ServerConfig> getServerConfig(String nodeId, String protocolId) throws FileNotFoundException {

        NetworkDomainConfigContext ndcc = SACTAEnvironment.getInstance().getNetworkDomainConfigContext();

        String domainId = SACTAEnvironment.getInstance().getMyDomainId();
        String centreId = SACTAEnvironment.getInstance().getMyCentreId();

        return getServerConfig(ndcc, domainId, centreId, nodeId, protocolId);
    }

    /**
     * Gets the server configuration.
     *
     * @param ndcc Network Domain Configuration Context
     * @param domainId Domain ID
     * @param centreId Centre ID
     * @param nodeId Node ID
     * @param protocolId Protocol ID
     * @return server configuration
     */
    public static List<ServerConfig> getServerConfig(
            NetworkDomainConfigContext ndcc,
            String domainId, String centreId, String nodeId, String protocolId) {

        List<ServerConfig> ret = new ArrayList<ServerConfig>();

        INode node = ndcc.getNode(nodeId);

        if (node == null) {
            log.log(Level.SEVERE,
                    "The node [" + nodeId + "] does not found in configuration file");
        } else {

            int port = -1;

            try {
                port = node.getPort(protocolId);

                List<String> ips = node.getIPs(domainId, centreId);

                //ips centre complementary
                ips.addAll(node.getIPs(domainId, SACTAEnvironment.getInstance().getMyCentreIdComplementary()));

                for (Enumeration<NetworkInterface> eIface = NetworkInterface.getNetworkInterfaces();
                        eIface.hasMoreElements();) {

                    NetworkInterface iface = eIface.nextElement();
                    Enumeration<InetAddress> eAddr = iface.getInetAddresses();

                    if (isIFace(iface.getInetAddresses(), ips)) {
                        while (eAddr.hasMoreElements()) {
                            InetAddress addr = eAddr.nextElement();
                            if (addr instanceof Inet4Address) {
                                ret.add(new ServerConfig(addr, port));
                            }
                        }
                    }
//					while (eAddr.hasMoreElements()) {
//
//						InetAddress addr = eAddr.nextElement();
//
//						String hostAddress = addr.getHostAddress();
//
//						if (ips != null && !ips.isEmpty()) {
//							for (String ip : ips) {
//								if (hostAddress.equalsIgnoreCase(ip)) {
//
//									ret.add(new ServerConfig(addr, port));
//
//									break;
//								}
//							}
//						}
//					}
                }
            } catch (Exception e) {
                if (port == -1) {
                    log.log(Level.SEVERE, "Undefined port for [" + node.getId() + "][" + protocolId + "]");
                } else {
                    log.log(Level.SEVERE, "", e);
                }
            }
        }

        return ret;
    }

    /**
     *
     * @param eAddr Enumeration<InetAddress>
     * @param ips List<String>
     * @return boolean
     */
    private static boolean isIFace(Enumeration<InetAddress> eAddr, List<String> ips) {
        boolean b = false;
        if (ips != null && !ips.isEmpty()) {
            while (eAddr.hasMoreElements()) {
                String ipRef = eAddr.nextElement().getHostAddress();
                for (String ip : ips) {
                    if (ipRef.equalsIgnoreCase(ip)) {
                        return true;
                    }
                }
            }
        }
        return b;
    }
//	public static NetworkDomainConfigContext getNetworkDomainConfigContext() {
//		ISCContext scContext = SCEngine.getInstance().getContext();
//
//		return scContext.getObjectProperty(
//				NetworkDomainConfigContext.class, SACTAContextConstants.NETWORK_DOMAIN_CONFIG_CTX_PROPERTY);
//	}
//	public static String getMyDomainId() {
//		return SCEngine.getInstance().getContext().getProperty(SACTAContextConstants.AGENT_DOMAIN_CTX_PROPERTY);
//	}
//
//	public static String getMyCentreId() {
//		return SCEngine.getInstance().getContext().getProperty(SACTAContextConstants.AGENT_CENTRE_CTX_PROPERTY);
//	}
}
