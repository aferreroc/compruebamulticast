/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import es.indra.scm.sca.sacta.compruebamulticast.config.Configuracion;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author aferrero
 */
public class EstadoCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        setEnabled(table == null || table.isEnabled()); // see question above 

//        if ((row % 2) == 0) {
//            setBackground(Color.blue);
//        } else {
//            setBackground(null);
//        }


        /*
         * public static final String CORRECTO = "Correcto"; public static final
         * Color CORRECTO_COLOR = Color.green; public static final String
         * INCORRECTO = "Incorrecto"; public static final Color INCORRECTO_COLOR
         * = Color.red; public static final String FALLO = "Fallo"; public
         * static final Color FALLO_COLOR = Color.orange; public static final
         * String INICIANDO = "Iniciando"; public static final Color
         * INICIANDO_COLOR = Color.blue; public static final String INTERRUMPIDO
         * = "Interrumpido"; public static final Color INTERRUMPIDO_COLOR =
         * null;
         */
        if (value instanceof String) {
            String valor = (String) value;
            if (valor.startsWith(Configuracion.CORRECTO)) {
                setBackground(Configuracion.CORRECTO_COLOR);
            } else if (valor.startsWith(Configuracion.AVISO)) {
                setBackground(Configuracion.AVISO_COLOR);
            } else if (valor.startsWith(Configuracion.INCORRECTO)) {
                setBackground(Configuracion.INCORRECTO_COLOR);
            } else if (valor.startsWith(Configuracion.FALTACABECERAS)) {
                setBackground(Configuracion.FALTACABECERAS_COLOR);
            } else if (valor.startsWith(Configuracion.FALLO)) {
                setBackground(Configuracion.FALLO_COLOR);
            } else if (valor.startsWith(Configuracion.INICIANDO)) {
                setBackground(Configuracion.INICIANDO_COLOR);
            } else if (valor.startsWith(Configuracion.INTERRUMPIDO)) {
                setBackground(Configuracion.INTERRUMPIDO_COLOR);
            } else {
                setBackground(null);
            }
        } else {
            setBackground(null);
        }

        super.getTableCellRendererComponent(table, value, selected, focused, row, column);

        return this;
    }
}
