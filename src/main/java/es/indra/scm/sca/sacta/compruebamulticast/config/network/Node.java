/**
 * *****************************************************************************
 * Name : es.indra.scm.sacta.config.Node
 *
 * Project : [SC|SGU|PSI|...] Subproject: spv-alm CSCI : CSC :
 * ****************************************************************************** Last change info.
 * ****************************************************************************** #$Author: dmoya $ #$Date: 2011-09-16 11:59:51 +0200 (vie,
 * 16 sep 2011) $ #$Revision: 51341 $ ******************************************************************************
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class
 * <code>Node</code> implements
 * <code>INode</code>.
 */
public class Node extends IdentificableObject implements INode {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * String IP Part.
     */
    private String ipPart;
    /**
     * List of parents.
     */
    private ArrayList<INode> parents;
    /**
     * List of children.
     */
    private ArrayList<INode> children;
    /**
     * List of static IP.
     */
    private ArrayList<String> staticIPsLan1, staticIPsLan2;
    /**
     * List of ports.
     */
    private HashMap<String, Integer> ports = new HashMap<String, Integer>();
    /**
     *
     */
    private NetworkDomainConfigContext networkDomainConfigContext;

    /**
     * Constructor used fields networkDomainConfigContext and id.
     *
     * @param networkDomainConfigContext Network Domain Configuration Context
     * @param id Identifier.
     */
    public Node(NetworkDomainConfigContext networkDomainConfigContext, String id) {
        setId(id);
        this.networkDomainConfigContext = networkDomainConfigContext;
    }

    
    public List<String> getIPs(String domainId, String centreId) {

        List<String> ret = null;

        IDomain domain = networkDomainConfigContext.getDomain(domainId);

        if (domain != null) {
            ICentre centre = domain.getCentre(centreId);

            ret = getIPs(domain, centre);
        }

        return (ret == null ? Collections.<String>emptyList() : ret);
    }

    
    public List<String> getIPs(ICentre centre) {
        return getIPs(centre.getDomain(), centre);
    }

    /**
     * Return list of ip through the domain and the centre.
     *
     * @param domain Domain
     * @param centre Centree
     * @return list of IP
     */
    public List<String> getIPs(IDomain domain, ICentre centre) {

        ArrayList<String> ret = null;

        if (domain != null && centre != null) {

            for (IPPartsPair pair : centre.getIPPartsPairs()) {
                String ip = null;

                if (isGroup()) {
                    ip = pair.getMulticastIP(this);
                } else {
                    ip = pair.getUnicastIP(domain, this);
                }

                if (ret == null) {
                    ret = new ArrayList<String>();
                }

                ret.add(ip);
            }
            //
            //			if (isGroup()) {
            //				for (String centreIpPart : centre.getMulticastIPParts()) {
            //					String ip = centreIpPart + "." + this.ipPart;
            //
            //					if (ret == null) {
            //						ret = new ArrayList<String>();
            //					}
            //
            //					ret.add(ip);
            //				}
            //			}
            //			else {
            //				for (String centreIpPart : centre.getUnicastIPParts()) {
            //					String ip = domain.getIPPart() + "." + centreIpPart + "." + this.ipPart;
            //
            //					if (ret == null) {
            //						ret = new ArrayList<String>();
            //					}
            //
            //					ret.add(ip);
            //				}
            //			}
        }

        return (ret == null ? Collections.<String>emptyList() : ret);
    }

    
    public String getIPPart() {
        return ipPart;
    }

    
    public void setIPPart(String ipPart) {
        this.ipPart = ipPart;
    }

    /**
     * Adds a port to the list of ports.
     *
     * @param protocolId Protocol ID
     * @param port Port
     */
    public void addPort(String protocolId, int port) {
        ports.put(protocolId, port);
        //System.out.println("addPort; protocolId: " + protocolId+" port: " +port);
    }

    /**
     * Return a port to the list of ports.
     *
     * @param protocolId Protocol ID
     * @return port.
     */
    
    public int getPort(String protocolId) {

        Integer port = ports.get(protocolId);

        return (port != null ? port : -1);
    }

    
    public List<INode> getChildren() {
        return (children == null ? Collections.<INode>emptyList() : children);
    }

    
    public void addChild(INode node) {
        if (children == null) {
            children = new ArrayList<INode>();
        }

        children.add(node);
    }

    
    public List<INode> getParents() {
        return (parents == null ? Collections.<INode>emptyList() : parents);
    }

    
    public void addParent(INode node) {
        if (parents == null) {
            parents = new ArrayList<INode>();
        }

        parents.add(node);
    }

    
    public void addStaticIP(String staticIP, int lan) {
        if (lan % 2 != 0) {
            if (staticIPsLan1 == null) {
                staticIPsLan1 = new ArrayList<String>();
            }

            staticIPsLan1.add(staticIP);
        } else {
            if (staticIPsLan2 == null) {
                staticIPsLan2 = new ArrayList<String>();
            }

            staticIPsLan2.add(staticIP);
        }
    }

    
    public List<String> getMulticastStaticIP(int lan) {
        List<String> lista = new ArrayList<String>();

        List<String> staticIPs = (lan % 2 != 0) ? staticIPsLan1 : staticIPsLan2;

        if (staticIPs != null) {
            for (String staticIP : staticIPs) {
                try {
                    InetAddress addr = InetAddress.getByName(staticIP);
                    if (addr.isMulticastAddress()) {
                        lista.add(staticIP);
                    }
                } catch (UnknownHostException ex) {
                    //
                }
            }
        }
        return lista;
    }

    
    public boolean isGroup() {
        return (children != null);
    }

    
    public void toXML(PrintWriter out) {
        if (!isGroup()) {
            out.println("<node " + XMLATTR_ID + "=\"" + getId() + "\" ipPart=\"" + ipPart + "\"/>");
        }
    }
}
