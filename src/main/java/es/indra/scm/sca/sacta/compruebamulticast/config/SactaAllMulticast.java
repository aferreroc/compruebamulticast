/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config;

import es.indra.scm.sca.sacta.compruebamulticast.ParGrupoMulticast;
import es.indra.scm.sca.sacta.compruebamulticast.config.network.SACTAEnvironment;
import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author aferrero
 */
public class SactaAllMulticast {

    private static final Logger log = Logger.getLogger(SactaAllMulticast.class.getName());
    private static final Pattern pattPuertos = Pattern.compile("\\* -> (\\w+)[ |\\u0009]+(\\d+).*");
    private static final Pattern pattGruposCompleto =
            Pattern.compile("(\\w+)[ |\\u0009]+(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})[ |\\u0009]+(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}).*");
    //los grupos de 2 octetos, el 1 octecto es siempre superior a 100
    private static final Pattern pattGruposShort =
            Pattern.compile("(\\w+)[ |\\u0009]+(\\d{3}\\.\\d{1,3}).*");
    private static final Pattern pattDominio =
            Pattern.compile("\\w+[ |\\u0009]+\\d+[ |\\u0009]+(\\w+).*");
    //                ID      101 ACC_LECM_R
    //                ID          1 ACC_LECM
    private static final Pattern pattCentro =
            Pattern.compile("[ |\\u0009]+ID[ |\\u0009]+\\d+[ |\\u0009]+(\\w+).*");
    //                IP      0.10.8.0  0.10.9.0    225.10.0.0  225.110.0.0
    private static final Pattern pattCentroIP =
            Pattern.compile("[ |\\u0009]+IP[ |\\u0009]+"
            + "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})" //IP LAN1
            + "[ |\\u0009]+"
            + "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})" //IP LAN2
            + "[ |\\u0009]+"
            + "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})" //MCAST LAN1
            + "[ |\\u0009]+"
            + "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}).*"); //MCAST LAN2
    private static final Pattern pattIP = Pattern.compile("(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})");
    private static final Pattern pattIP_2octetos = Pattern.compile("(\\d{1,3})\\.(\\d{1,3})");
    //
//    pos_tma       pos_tma1, pos_tma2, pos_tma3, pos_tma4, pos_tma5,
//              pos_tma6, pos_tma7, pos_tma8, pos_tma9, pos_tma10,
//              pos_tma11, pos_tma12, pos_tma13, pos_tma14, pos_tma15,
//              pos_tma16, pos_tma17, pos_tma18, pos_tma19, pos_tma20
    private static final Pattern pattCompoGrupos = Pattern.compile("(\\w{3,})[ |\\u0009]+([\\w{3,}[ |\\u0009]*,[ |\\u0009]*|\\w{3,} *]+)");
    private static final Pattern pattCompoGruposSiguente = Pattern.compile("[ |\\u0009]+([\\w{3,}[ |\\u0009]*,[ |\\u0009]*|\\w{3,} *]+)");
    private static final String DOMINIO = "SACTA";

    /**
     * Todos los grupos multicast del fichero de configuracion
     *
     * @param ficheroConfiguracion
     * @return Mapa key=nodo, value=grupos multicast o parte del grupo multicast
     */
    public static Map<String, ParGrupoMulticast> dameSactaAllMulticast(String centro, String ficheroConfiguracion) throws IOException {

        System.err.println("Fichero configuracion: " + ficheroConfiguracion);

        TreeMap<String, ParGrupoMulticast> gruposMulticast = new TreeMap<String, ParGrupoMulticast>();



        boolean isDominioDetectado = false;
        boolean isCentroDetectado = false;
        boolean isIPCentroDetectado = false;

        boolean sigienteLineaGrupos = false;
        Map<String, List<String>> composicionGrupos = new HashMap<String, List<String>>();
        //
        String ipMcastLan1 = "";
        String ipMcastLan2 = "";
        Matcher m;
        // Open the file that is the first 
        // command line parameter
        FileInputStream fstream = new FileInputStream(ficheroConfiguracion);
        // Get the object of DataInputStream
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        //Read File Line By Line
        while ((strLine = br.readLine()) != null) {
            //
            //Tratamiento grupos multicast
            // Formamos un map de los grupos multicast
            String grupoActualProcesando = null;

            m = pattCompoGrupos.matcher(strLine);
            if (m.matches()) {

                grupoActualProcesando = m.group(1);
                String[] tempNodosGrupo = m.group(2).split(",");
                String[] nodosGrupo;

                if (tempNodosGrupo[tempNodosGrupo.length - 1] == null) {
                    sigienteLineaGrupos = true;
                    nodosGrupo = new String[tempNodosGrupo.length - 1];
                    System.arraycopy(tempNodosGrupo, 0, nodosGrupo, 0, tempNodosGrupo.length - 1);
                } else {
                    sigienteLineaGrupos = false;  //se ha acabado el grupo, no se procesan mas
                    nodosGrupo = tempNodosGrupo;
                }
                composicionGrupos.put(grupoActualProcesando, Arrays.asList(nodosGrupo));
                //System.out.println("Grupo: " + m.group(1) + " " + nodosGrupo.length);
            }
            //
            //

            if (sigienteLineaGrupos) { //seguimos procesando lineas de grupo
                m = pattCompoGruposSiguente.matcher(strLine);
                if (m.matches()) {

                    String[] tempNodosGrupo = m.group(1).split(",");
                    String[] nodosGrupo;

                    if (tempNodosGrupo[tempNodosGrupo.length - 1] == null) {
                        sigienteLineaGrupos = true;
                        nodosGrupo = new String[tempNodosGrupo.length - 1];
                        System.arraycopy(tempNodosGrupo, 0, nodosGrupo, 0, tempNodosGrupo.length - 1);
                    } else {
                        sigienteLineaGrupos = false;
                        nodosGrupo = tempNodosGrupo;
                    }
                    //System.out.println("Grupo(cont): " + nodosGrupo.length);
                    List<String> listaActual = composicionGrupos.get(grupoActualProcesando);
                    listaActual.addAll(Arrays.asList(nodosGrupo));
                    composicionGrupos.put(grupoActualProcesando, listaActual);
                }
            }


            //Detectamos dominio
            if (!isDominioDetectado) {
                m = pattDominio.matcher(strLine);
                if (m.matches() && m.group(1).contains(DOMINIO)) {
                    isDominioDetectado = true;
                    System.out.println("Dominio= " + m.group(1));
                }

            }
            //Detectamos centro
            if (isDominioDetectado && !isCentroDetectado) {
                m = pattCentro.matcher(strLine);
                if (m.matches() && m.group(1).contains(centro)) {
                    isCentroDetectado = true;
                    System.out.println("Centro= " + m.group(1));
                }

            }
            //Detectamos IP Centro
            if (isDominioDetectado && isCentroDetectado && !isIPCentroDetectado) {
                m = pattCentroIP.matcher(strLine);
                if (m.matches()) {
                    isIPCentroDetectado = true;
                    System.out.println("IP= " + m.group(3) + " " + m.group(4));
                    ipMcastLan1 = m.group(3);
                    ipMcastLan2 = m.group(4);
                }

            }

            //Buscamos grupos y puertos
            if (isDominioDetectado && isCentroDetectado && isIPCentroDetectado) {
                //System.out.println(strLine);
                //fms_acc              224.1.100.100 224.2.100.100
                m = pattGruposCompleto.matcher(strLine);
                if (m.matches()) {
                    gruposMulticast.put(m.group(1), new ParGrupoMulticast(m.group(2), m.group(3), 0));
                    //System.out.println(m.group(1) + " " + m.group(2) + " " + m.group(3));
                } else {
                    //infais     108.5
                    m = pattGruposShort.matcher(strLine);
                    if (m.matches()) {
                        gruposMulticast.put(m.group(1), mezclaIPmcast(ipMcastLan1, ipMcastLan2, m.group(2)));
                        //System.out.println(m.group(1) + " " + m.group(2));
                    } else {
                        //LOS PUERTOS 
                        //* -> pos_tma20    19029
                        m = pattPuertos.matcher(strLine);
                        if (m.matches()) {
                            //System.out.println(m.group(1) + " " + m.group(2));
                            String grupo = m.group(1);
                            String puerto = m.group(2);

                            boolean grupoEncontrado = false;
                            ParGrupoMulticast parGrMcast = gruposMulticast.get(grupo);
                            if (parGrMcast != null) {
                                parGrMcast.puerto = Integer.parseInt(puerto);
                                gruposMulticast.put(grupo, parGrMcast);
                                grupoEncontrado = true;
                            }

                            //añado tambien el puerto a los grupos """"
                            for (String grupoLista : composicionGrupos.keySet()) {
                                if (composicionGrupos.get(grupoLista).contains(grupo)) {
                                    parGrMcast = gruposMulticast.get(grupoLista);
                                    if (parGrMcast != null) {
                                        parGrMcast.puerto = Integer.parseInt(puerto);
                                        gruposMulticast.put(grupoLista, parGrMcast);
                                        grupoEncontrado = true;
                                    }
                                }
                            }


                            if (!grupoEncontrado) {
                                //log.info("no he encontrado el grupo multicast para: " + grupo + " (" + puerto + ")");
                            }
                        }
                    }
                }
            }
        }
        //Close the input stream
        in.close();

//        for (String grupo : composicionGrupos.keySet()) {
//            StringBuilder sb = new StringBuilder(grupo).append(": ");
//            for (String nodo : composicionGrupos.get(grupo)) {
//                sb.append(nodo).append(" ");
//            }
//            System.out.println(sb.toString());
//        }



        return gruposMulticast;
    }

    private static ParGrupoMulticast mezclaIPmcast(String grLan1, String grLan2, String grupoDetectado) {
        Matcher mLan1 = pattIP.matcher(grLan1);
        Matcher mLan2 = pattIP.matcher(grLan2);
        Matcher grDetect = pattIP_2octetos.matcher(grupoDetectado);
        if (mLan1.matches() && mLan2.matches() && grDetect.matches()) {
            String ipLan1 = mLan1.group(1) + "." + mLan1.group(2) + "." + grDetect.group(1) + "." + grDetect.group(2);
            String ipLan2 = mLan2.group(1) + "." + mLan2.group(2) + "." + grDetect.group(1) + "." + grDetect.group(2);
            return new ParGrupoMulticast(ipLan1, ipLan2, 0);
        } else {
            throw new NumberFormatException("IP no correcta");
        }
    }

    private static void generaFicheroConfiguracion(String ficheroConfiguracion, Map<String, ParGrupoMulticast> grLan1, Map<String, ParGrupoMulticast> grLan2) throws IOException {

        //Definimos los stream de salida
        FileOutputStream fstream = new FileOutputStream(ficheroConfiguracion);


        //Primero copiamos la plantilla en el fichero de salida
        InputStream in = SactaAllMulticast.class.getResourceAsStream(
                "/es/indra/scm/sca/sacta/compruebamulticast/config/plantillaMulticast.conf");


        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            fstream.write(buf, 0, len);
        }
        in.close();
        //out.close();
        fstream.flush();


        //Pasamo
        DataOutputStream out = new DataOutputStream(fstream);
        BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(out));

        wr.newLine();
        wr.newLine();
        wr.newLine();
        wr.flush();

        int contador = 1;
        for (ParGrupoMulticast parGrMcast : grLan1.values()) {
            //R_MCAST_1=229.40.104.48;229.140.104.48;21411
            StringBuilder sb = new StringBuilder("C_MCAST_").append(contador).append("=");
            sb.append(parGrMcast.GR1).append(";");
            sb.append(parGrMcast.GR2).append(";");
            sb.append(parGrMcast.puerto).append("\n");
            contador++;

            wr.append(sb);
        }
        wr.newLine();
        wr.flush();
        //
        contador = 1;
        for (ParGrupoMulticast parGrMcast : grLan2.values()) {
            //R_MCAST_1=229.40.104.48;229.140.104.48;21411
            StringBuilder sb = new StringBuilder("R_MCAST_").append(contador).append("=");
            sb.append(parGrMcast.GR1).append(";");
            sb.append(parGrMcast.GR2).append(";");
            sb.append(parGrMcast.puerto).append("\n");
            contador++;

            wr.append(sb);
        }
        wr.flush();
        wr.close();

    }

    public static void main(String[] args) {

        //Leo configuracion
        String rutaSACTA;


        if (args.length == 1) {
            rutaSACTA = args[0].trim();
        } else {
            System.out.println("Use: multicastgenfichero.sh <ruta ficheros SACTA>");
            return;
        }

        System.out.println("Leyendo configuraciones de: " + rutaSACTA);
        try {

            //--------------------------------------------------------------------------------------------------------------------------------
            Properties props = System.getProperties();
            props.setProperty("java.net.preferIPv4Stack", "true");
            System.setProperties(props);

            //
            String fichC = rutaSACTA + File.separator + "COM_IP_CENTROS.CFG";
            String fichR = rutaSACTA + File.separator + "COM_IP_CENTROS.CFG_radar";

            String[] centros = {"ACC_LECM", "ACC_LECS", "ACC_LECB", "ACC_GCCC", "TMA_LEPA"};


            System.out.println("==================================================");

            for (String centro : centros) {

                Map<String, ParGrupoMulticast> grC = dameSactaAllMulticast(centro, fichC);
                Map<String, ParGrupoMulticast> grR = dameSactaAllMulticast(centro + "_R", fichR);
                generaFicheroConfiguracion("multicast" + centro + ".conf", grC, grR);

                System.out.println("Totales Lan1: " + grC.size() + " Lan2: " + grR.size());
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
