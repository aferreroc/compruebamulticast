/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import es.indra.scm.sca.sacta.compruebamulticast.config.Configuracion;
import es.indra.scm.sca.sacta.compruebamulticast.utils.ScrollableDialog;
import es.indra.scm.sca.sacta.compruebamulticast.utils.StringUtils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author aferrero
 */
public class McastTableModel extends AbstractTableModel {

    private static final Logger log = Logger.getLogger(McastTableModel.class.getName());
    private static final long serialVersionUID = 1L;
    private boolean esControl = true;
    private LinkedList<ParGrupoMulticast> datos = new LinkedList<ParGrupoMulticast>();
    private ArrayList<DatoFalloPaquete> listaFallosPaquets = new ArrayList<DatoFalloPaquete>();
    //private final LinkedBlockingQueue<DatoComprobacion> colaComprobaciones;
    private final DecimalFormat df = new DecimalFormat("##0.00");
    private static final String[] COLUMN_NAMES = new String[]{"Grupo Multicast LAN 1", "Grupo Multicast LAN 2", "Puerto", "Estado",
        "Lan 1 (num. paquetes)", "Lan 2 (num. paquetes)", "Avisos", "Fallos", "Porcentaje fallos", "Fuentes Lan 1", "Fuentes Lan 2", "Tabla Fuentes"};
    private static final Class<?>[] COLUMN_TYPES = new Class<?>[]{String.class, String.class, Integer.class, String.class,
        Integer.class, Integer.class, Integer.class, Integer.class, String.class, Integer.class, Integer.class, JButton.class};

    protected static class DatoComprobacion {

        String comprobacion;
        int row;

        public DatoComprobacion(String comprobacion, int row) {
            this.comprobacion = comprobacion;
            this.row = row;
        }
    }

    protected static class DatoFalloPaquete {

        int fallos, avisos, numPaquetesLan1, numPaquetesLan2;

        public DatoFalloPaquete() {
            this.avisos = 0;
            this.fallos = 0;
            this.numPaquetesLan1 = 0;
            this.numPaquetesLan2 = 0;
        }
    }

    public McastTableModel(boolean esControl) {
        this.esControl = esControl;
        //colaComprobaciones = new LinkedBlockingQueue<DatoComprobacion>();
    }

    public int getColumnCount() {
        return 12;
    }

    public int getRowCount() {
        return datos.size();
    }

    private String damePorcentaje(int rowIndex) {

        double porcentaje = 0.0;
        int paquetes;

        if (listaFallosPaquets.get(rowIndex).numPaquetesLan1 > listaFallosPaquets.get(rowIndex).numPaquetesLan2) {
            paquetes = listaFallosPaquets.get(rowIndex).numPaquetesLan1;
        } else {
            paquetes = listaFallosPaquets.get(rowIndex).numPaquetesLan2;
        }

        if (paquetes > 0) {
            porcentaje = (100.0 * listaFallosPaquets.get(rowIndex).fallos) / paquetes;
            if (porcentaje > 100) {
                porcentaje = 100.0;
            }
        }

        return df.format(porcentaje);
    }

    public Object getValueAt(final int rowIndex, final int columnIndex) {

        final ParGrupoMulticast parGrMcast = datos.get(rowIndex);

        // Se obtiene el campo apropiado según el valor de columnIndex
        switch (columnIndex) {
            case 0:
                return parGrMcast.GR1;
            case 1:
                return parGrMcast.GR2;
            case 2:
                return parGrMcast.puerto;
            case 3:
                return parGrMcast.estado;
            case 4:
                return listaFallosPaquets.get(rowIndex).numPaquetesLan1;
            case 5:
                return listaFallosPaquets.get(rowIndex).numPaquetesLan2;
            case 6:
                return listaFallosPaquets.get(rowIndex).avisos;
            case 7:
                return listaFallosPaquets.get(rowIndex).fallos;
            case 8:
                return damePorcentaje(rowIndex);
            case 9:
                return parGrMcast.getFuentesLan1().size();
            case 10:
                return parGrMcast.getFuentesLan2().size();
            case 11:
                final JButton button = new JButton(COLUMN_NAMES[columnIndex]);
                button.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent arg0) {
                        StringBuilder sb = new StringBuilder("Fuentes Lan 1 (").append(parGrMcast.GR1).append("):\n");
                        for (String fuente : parGrMcast.getFuentesLan1()) {
                            sb.append(fuente).append("\n");
                        }

                        sb.append("\nFuentes Lan 2 (").append(parGrMcast.GR2).append("):\n");
                        for (String fuente : parGrMcast.getFuentesLan2()) {
                            sb.append(fuente).append("\n");
                        }

                        //JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(button), sb.toString());
                        ScrollableDialog dialog = new ScrollableDialog(JOptionPane.getFrameForComponent(button), sb.toString());
                        dialog.showMessage();
                    }
                });
                return button;
            default:
                return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override
    public String getColumnName(int columnIndex) {
        // Devuelve el nombre de cada columna. Este texto aparecerá en la
        // cabecera de la tabla.
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    /**
     * Añade una persona al final de la tabla
     *
     * @param nuevoMcast
     * @return la posicion del elemento añadido
     */
    public int addMcast(ParGrupoMulticast nuevoMcast) {
        datos.add(nuevoMcast);
        listaFallosPaquets.add(new DatoFalloPaquete());

        //log.info("addMcast; " + (datos.size()-1));
        fireTableDataChanged();
        return (datos.size() - 1);
    }

    public void incrementaAviso(int row) {
        int avisosActuales = listaFallosPaquets.get(row).avisos + 1;
        listaFallosPaquets.get(row).avisos = avisosActuales;
        fireTableCellUpdated(row, 6); //avisos
    }

    public void incrementaFallo(int row) {
        int fallosActuales = listaFallosPaquets.get(row).fallos + 1;

        int res;
        if (listaFallosPaquets.get(row).numPaquetesLan1 > listaFallosPaquets.get(row).numPaquetesLan2) {
            res = listaFallosPaquets.get(row).numPaquetesLan1 - listaFallosPaquets.get(row).numPaquetesLan2;
        } else {
            res = listaFallosPaquets.get(row).numPaquetesLan2 - listaFallosPaquets.get(row).numPaquetesLan1;
        }

        if (fallosActuales > res) {
            fallosActuales = res;
        }
        listaFallosPaquets.get(row).fallos = fallosActuales;


        fireTableCellUpdated(row, 7); //fallos
        fireTableCellUpdated(row, 8); //porcentaje

    }

    public void incrementaPaquete(int row, boolean esLan1) {
        if (esLan1) {
            int paquetes = listaFallosPaquets.get(row).numPaquetesLan1 + 1;
            listaFallosPaquets.get(row).numPaquetesLan1 = paquetes;
        } else {
            int paquetes = listaFallosPaquets.get(row).numPaquetesLan2 + 1;
            listaFallosPaquets.get(row).numPaquetesLan2 = paquetes;
        }
//        fireTableCellUpdated(row, 5); //paquetes
//        fireTableCellUpdated(row, 6); //porcentaje
        fireTableRowsUpdated(row, row);
    }

    public void verificaEstadoLinea(int row) {
        String texto = (String) getValueAt(row, 3);

        if ((texto == null || StringUtils.isEmpty(texto) || texto.trim().startsWith(Configuracion.INICIANDO)) && listaFallosPaquets.get(row).numPaquetesLan1 > 0
                && listaFallosPaquets.get(row).numPaquetesLan2 > 0
                && listaFallosPaquets.get(row).avisos == 0
                && listaFallosPaquets.get(row).fallos == 0) {

            datos.get(row).estado = Configuracion.CORRECTO;
            fireTableCellUpdated(row, 3);
        }
    }

    public void setComprobacion(String texto, int row) {

        if (texto.startsWith(Configuracion.FALLO)) {
            //setValueAt(texto, row, 3);
            datos.get(row).estado = texto;
            fireTableCellUpdated(row, 3);
        } else if (getValueAt(row, 3) == null
                || (!((String) getValueAt(row, 3)).startsWith(Configuracion.FALLO)
                && !((String) getValueAt(row, 3)).startsWith(Configuracion.INCORRECTO))) {
            //setValueAt(texto, row, 3);
            datos.get(row).estado = texto;
            fireTableCellUpdated(row, 3);
        }
    }

    public void limpiaContadores() {
        for (int row = 0; row < datos.size(); row++) {
            //setValueAt(Configuracion.INICIANDO, row, 3);
            datos.get(row).estado = Configuracion.INICIANDO;
            listaFallosPaquets.get(row).avisos = 0;
            listaFallosPaquets.get(row).fallos = 0;
            listaFallosPaquets.get(row).numPaquetesLan1 = 0;
            listaFallosPaquets.get(row).numPaquetesLan2 = 0;
            datos.get(row).limpiaFuentesLan();
        }

        fireTableDataChanged();
    }
}
