/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.util.StringTokenizer;

/**
 *
 * @author aferrero
 */
public class SACTAAgentNamingUtils {

    public static String convertLabelIdToComIPCentrosId(String label) {
        // POS_TMA_B_6 ---> POS_TMA_B6   
        // TDR_1 ---> TDR1
        if (label == null) {
            return null;
        }
        
        //System.out.println("aaa->  " + label);
        StringTokenizer strTok = new StringTokenizer(label, "_");

        if (strTok.countTokens() > 3) {
            StringBuilder strBuilder = new StringBuilder();
            int i = 0;
            while (strTok.hasMoreTokens()) {
                if (i < 2) {
                    strBuilder.append(strTok.nextToken()).append("_");
                    i++;
                } else {
                    strBuilder.append(strTok.nextToken());
                }
//                System.out.println(strBuilder.toString()+  "    " + i);
            }
            //System.out.println("bbb->  " + strBuilder.toString());
            return strBuilder.toString();
        } else {
            //System.out.println("bbb->  " + label);
            return label;
        }
        
        

    }
}
