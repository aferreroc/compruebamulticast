/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.utils;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class ScrollableDialog{
    JDialog dialog;
    JTextArea textArea;
    int dialogResult = JOptionPane.CANCEL_OPTION;
    
    public ScrollableDialog(Frame owner, String text){
        // create a modal dialog that will block until hidden
        dialog = new JDialog(owner, true);
        dialog.setVisible(false);
        dialog.setLayout(new BorderLayout());
        
        textArea = new JTextArea(text);
        textArea.setLineWrap(true);
        textArea.setEditable(false);
        
        JScrollPane scrollpane = new JScrollPane(textArea);
        dialog.add(scrollpane,BorderLayout.CENTER);
        
        JButton btnOk = new JButton("Ok");
        btnOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // This will "close" the modal dialog and allow program
                // execution to continue.
                dialogResult = JOptionPane.OK_OPTION;
                dialog.setVisible(false);
                dialog.dispose();
            }
        });
        dialog.add(btnOk,BorderLayout.SOUTH);
        
        dialog.setSize(300,400);
        dialog.setLocationRelativeTo(owner);
    }
    
    public int showMessage(){
        // show the dialog - this will block until setVisible(false) occurs
        dialog.setVisible(true);
        // return whatever data is required
        return dialogResult;
    }
    
    
}
