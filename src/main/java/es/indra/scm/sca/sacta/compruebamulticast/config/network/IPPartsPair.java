/*******************************************************************************
 * Name      : es.indra.scm.sacta.config.IPPartsPair
 * Project   : [SC|SGU|PSI|...]
 * Subproject: spv-alm
 * CSCI      :
 * CSC       :
 *******************************************************************************
 * Last change info.
 *******************************************************************************
 * #$Author: dmoya $
 * #$Date: 2011-09-16 11:59:51 +0200 (vie, 16 sep 2011) $
 * #$Revision: 51341 $
 *******************************************************************************
 */

package es.indra.scm.sca.sacta.compruebamulticast.config.network;


/**
 * Class <code>IPartPair</code>. It contains the ip address configuration<br>
 * (Unicast/Multicast).
 */
public class IPPartsPair {

	/** String Unicast IP Part. */
	private String unicastIPPart;

	/** String Multicast IP Part. */
	private String multicastIPPart;
        
        private int lan;

	/**
	 * Default Constructor.
	 * @param unicastIPPart UnicastIPPart
	 * @param multicastIPPart MulticastIPPart
	 */
	public IPPartsPair(String unicastIPPart, String multicastIPPart, int lan) {
		this.unicastIPPart = unicastIPPart;
		this.multicastIPPart = multicastIPPart;
                this.lan = lan;
	}

	/**
	 * Gets the octet unicast ip address.
	 * @return String UnicastIPPart
	 */
	public String getUnicastIPPart() {
		return unicastIPPart;
	}


	/**
	 * Gets the octet multicast ip address.
	 * @return String Multicast IP Part.
	 */
	public String getMulticastIPPart() {
		return multicastIPPart;
	}

	/**
	 * Gets the IP address unicast.
	 * @return String Unicast IP
	 */
	public String getUnicastIP() {
		String ret = null;

		return ret;
	}

	/**
	 * Gets the ip address unicast through the
	 * <code>IDomain</code> and <code>INode</code>.
	 * @param domain Domain
	 * @param node Node
	 * @return String Unicast IP
	 */
	public String getUnicastIP(IDomain domain, INode node) {
		return domain.getIPPart() + "." + getUnicastIPPart() + "." + node.getIPPart();
	}

	/**
	 * Gets the ip address multicast through the
	 * <code>INode</code>.
	 * @param node Node
	 * @return String Multicast IP
	 */
	public String getMulticastIP(INode node) {
		return getMulticastIPPart() + "." + node.getIPPart();
	}
        
        public int getLan() {
            return lan;
        }
}
