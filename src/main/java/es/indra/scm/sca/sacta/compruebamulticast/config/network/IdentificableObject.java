/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

/**
 *
 * @author aferrero
 */
public abstract class IdentificableObject implements IIdentificableObject {

    public static final String XMLATTR_ID = "1.0";
    /**
     * String Id.
     */
    private String id;

    public void setId(String id) {
        this.id = id;
    }
    
    
    public String getId() {
        return id;
    }
}
