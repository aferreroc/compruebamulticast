/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.io.PrintWriter;

/**
 *
 * @author aferrero
 */
public interface IXMLObjectWriter {
    void toXML(PrintWriter out);
}
