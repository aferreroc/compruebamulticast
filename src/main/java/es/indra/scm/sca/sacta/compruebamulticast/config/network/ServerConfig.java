/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.net.InetAddress;

/**
 *
 * @author aferrero
 */
public class ServerConfig {

    private InetAddress addr;
    private int port;

    public ServerConfig(InetAddress addr, int port) {
        this.addr = addr;
        this.port = port;
    }
    
    
}
