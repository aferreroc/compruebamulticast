/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.util.Comparator;

/**
 *
 * @author aferrero
 */
public class MulticastServerConfigComparator implements Comparator<MulticastServerConfig> {
    
    
    public int compare(MulticastServerConfig primero, MulticastServerConfig otro) {
        int compara = primero.getAddr().toString().compareTo(otro.getAddr().toString());
        if (compara == 0) {
            compara = primero.getMulticastIP().compareTo(otro.getMulticastIP());
            if (compara == 0) {
                if (primero.getPort() == otro.getPort()) {
                    compara = 0;
                } else if (primero.getPort() > otro.getPort()) {
                    compara = 1;
                } else {
                    compara = -1;
                }
            }
        }
        return compara;
    }
    
}
