/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import es.indra.scm.sca.sacta.compruebamulticast.config.Configuracion;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aferrero Consumer Thread
 */
public class MulticastComparadorRunnable implements Runnable {

    private static final Logger log = Logger.getLogger(MulticastComparadorRunnable.class.getName());
    private final BlockingQueue<Secuencia> queue;
    private McastTableModel tableModel;
    private int row;
    private ParGrupoMulticast parGrupoMulticast;
    private boolean isRunning = true;
    private int tiempoRefrescoColaSecs = 5;
    private long difTiempoSecuenciasMills = 700;
    private Map<String, SecuenciasRecibidas> mapaSecuencias;

    public MulticastComparadorRunnable(BlockingQueue<Secuencia> queue,
            McastTableModel tableModel, int row, int tiempoRefrescoColaSecs, long difTiempoSecuenciasMills, ParGrupoMulticast parGrupoMulticast) {
        this.queue = queue;
        this.tableModel = tableModel;
        this.row = row;
        this.tiempoRefrescoColaSecs = tiempoRefrescoColaSecs;
        this.difTiempoSecuenciasMills = difTiempoSecuenciasMills;
        this.parGrupoMulticast = parGrupoMulticast;

        tableModel.setComprobacion(Configuracion.INICIANDO, row);

        mapaSecuencias = new HashMap<String, SecuenciasRecibidas>();
    }

    public void start() {
        log.log(Level.INFO, "Iniciada fila: {0}", row);
        isRunning = true;
        tableModel.setComprobacion(Configuracion.INICIANDO, row);
    }

    public void stop() {
        log.log(Level.INFO, "Parada fila: {0}", row);
        isRunning = false;
        tableModel.setComprobacion(Configuracion.INTERRUMPIDO, row);
    }

    public void run() {
        isRunning = true;
        try {
            //al principio paramos un poco para que se vaya cargando la tabla       
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            tableModel.setComprobacion(Configuracion.INTERRUMPIDO + " " + ex.getMessage(), row);
            log.log(Level.SEVERE, null, ex);
        }


        tableModel.setComprobacion(Configuracion.INICIANDO, row);

        long ahora;
        long ultimaRevisionCola = 0;

        try {

            while (true) {

                if (!isRunning) {
                    Thread.sleep(2000);
                }

                Secuencia secuenciaObtenida = queue.poll(tiempoRefrescoColaSecs, TimeUnit.SECONDS);
                //Secuencia secuenciaObtenida = queue.take();



                //el tiempo
                ahora = Calendar.getInstance().getTimeInMillis();

                if (secuenciaObtenida != null) {


                    if (secuenciaObtenida.secuencia >= SecuenciasRecibidas.MAX_LENGTH) {
                        log.log(Level.WARNING, "Una secuencia con valor de " + secuenciaObtenida.secuencia + " no no puede ser! Secuencia:"
                                + (secuenciaObtenida.isLan1 ? " LAN 1 GrupoMulticast: " + parGrupoMulticast.GR1 : " LAN 2 GrupoMulticast: " + parGrupoMulticast.GR2)
                                + " Origen: " + secuenciaObtenida.hostOrigen + " timestamp: " + ahora + " fila: " + row);
                        continue;
                    }

                    if (secuenciaObtenida.secuencia != Secuencia.NO_NUMERO && secuenciaObtenida.secuencia != Secuencia.ERROR) {

                        //Anadimos el origen de la secuencia
                        parGrupoMulticast.addFuenteLan(secuenciaObtenida.isLan1, secuenciaObtenida.hostOrigen);

                        //Sacamos el ultimo octeto del host origen
                        String lastOctet = secuenciaObtenida.hostOrigen.substring(secuenciaObtenida.hostOrigen.lastIndexOf(".") + 1);

                        SecuenciasRecibidas secuenciasRecibidas = mapaSecuencias.get(lastOctet);
                        if (secuenciasRecibidas == null) {
                            //System.out.println("Nuevo objeto secuencias recibidas para " + secuenciaObtenida.hostOrigen);
                            secuenciasRecibidas = new SecuenciasRecibidas(ahora, secuenciaObtenida);
                            mapaSecuencias.put(lastOctet, secuenciasRecibidas);
                        } else {
                            secuenciasRecibidas.setTimestamp(ahora, secuenciaObtenida);
                        }

                        tableModel.incrementaPaquete(row, secuenciaObtenida.isLan1);

                        log.log(Level.INFO, "Recibo secuencia: " + secuenciaObtenida.secuencia
                                + (secuenciaObtenida.isLan1 ? " LAN 1 GrupoMulticast: " + parGrupoMulticast.GR1 : " LAN 2 GrupoMulticast: " + parGrupoMulticast.GR2)
                                + " Origen: " + secuenciaObtenida.hostOrigen + " timestamp: " + ahora + " fila: " + row);


                        //(1) comprobamos si llega en orden
                        checkLlegaEnOrden(secuenciaObtenida, secuenciasRecibidas);
                        //(2) la diferencia entre L1 y L2 no debe superar difTiempoSecuenciasMills
                        checkDiferenciaCorrectaEnLANES(secuenciaObtenida, secuenciasRecibidas);

                        //Pone a correcto si es necesario
                        tableModel.verificaEstadoLinea(row);


                    } else if (secuenciaObtenida.secuencia == Secuencia.NO_NUMERO) {
                        tableModel.setComprobacion(
                                Configuracion.INCORRECTO + " no se ha recibido un numero de secuencia (error de cabecera) en la "
                                + (secuenciaObtenida.isLan1 ? " LAN 1 GrupoMulticast: " + parGrupoMulticast.GR1 : " LAN 2 GrupoMulticast: " + parGrupoMulticast.GR2)
                                + " Origen: " + secuenciaObtenida.hostOrigen, row);
                        tableModel.incrementaFallo(row);


                        log.info(Configuracion.INCORRECTO + " no se ha recibido un numero de secuencia (error de cabecera) en la "
                                + (secuenciaObtenida.isLan1 ? " LAN 1 GrupoMulticast: " + parGrupoMulticast.GR1 : " LAN 2 GrupoMulticast: " + parGrupoMulticast.GR2)
                                + " Origen: " + secuenciaObtenida.hostOrigen + " fila-> " + row);
                    } else {
                        tableModel.setComprobacion(
                                Configuracion.INCORRECTO + " error en la "
                                + (secuenciaObtenida.isLan1 ? " LAN 1 GrupoMulticast: " + parGrupoMulticast.GR1 : " LAN 2 GrupoMulticast: " + parGrupoMulticast.GR2)
                                + " Origen: " + secuenciaObtenida.hostOrigen, row);
                        tableModel.incrementaFallo(row);

                        log.info(Configuracion.INCORRECTO + " error en la "
                                + (secuenciaObtenida.isLan1 ? " LAN 1 GrupoMulticast: " + parGrupoMulticast.GR1 : " LAN 2 GrupoMulticast: " + parGrupoMulticast.GR2)
                                + " Origen: " + secuenciaObtenida.hostOrigen + " row-> " + row);

                    }

                    if ((ahora - ultimaRevisionCola) > (tiempoRefrescoColaSecs * 1000)) {
                        ultimaRevisionCola = revisamosCola();
                    }


                } else {
                    //revisamos la cola (cada waitSecuencias) 
                    // lo ponemos aqui por si acaso llegan muchas secuencias null
                    ultimaRevisionCola = revisamosCola();

                }
            }
        } catch (InterruptedException ex) {
            tableModel.setComprobacion(Configuracion.INTERRUMPIDO, row);
            log.log(Level.INFO, null, ex);
        }

    }

    private boolean checkDiferenciaCorrectaEnLANES(Secuencia secuenciaObtenida, SecuenciasRecibidas secuenciasRecibidas) {

        if (secuenciasRecibidas.isEsperandoOtraLAN()) {
            secuenciasRecibidas.setEsperandoOtraLAN(!secuenciasRecibidas.isEsperandoOtraLAN());

            return true;
        } else {

            boolean esCorrecto = (secuenciasRecibidas.getLastTimestampLan(secuenciaObtenida) - secuenciasRecibidas.getLastTimestampOtherLan(secuenciaObtenida))
                    <= difTiempoSecuenciasMills;


            if (!esCorrecto) {


//                if (secuenciasRecibidas.getTimestampLan(secuenciaObtenida) == -1L || secuenciasRecibidas.getTimestampOtherLan(secuenciaObtenida) == -1) {
//                    tableModel.setComprobacion(
//                            Configuracion.INCORRECTO + " no ha llegado a tiempo la secuencia "
//                            + secuenciaObtenida.secuencia + " a la LAN " + (!secuenciaObtenida.isLan1 ? "1" : "2") + " origen: " + secuenciaObtenida.hostOrigen, row);
//                    tableModel.incrementaFallo(row);
//
//                    log.info(Configuracion.INCORRECTO + " no ha llegado a tiempo la secuencia "
//                            + secuenciaObtenida.secuencia + " a la LAN " + (!secuenciaObtenida.isLan1 ? "1" : "2") + " origen: " + secuenciaObtenida.hostOrigen + " fila-> " + row);
//                
//             
//                
//                } else {
//                    tableModel.setComprobacion(
//                            Configuracion.INCORRECTO + " ha llegado mas de " + difTiempoSecuenciasMills + " ms tarde la secuencia "
//                            + secuenciaObtenida.secuencia + " en la LAN " + (secuenciaObtenida.isLan1 ? "1" : "2") + " origen: " + secuenciaObtenida.hostOrigen, row);
//                    tableModel.incrementaFallo(row);
//
//                    log.info(Configuracion.INCORRECTO + " ha llegado mas de " + difTiempoSecuenciasMills + " ms tarde la secuencia "
//                            + secuenciaObtenida.secuencia + " en la LAN " + (secuenciaObtenida.isLan1 ? "1" : "2") + " origen: " + secuenciaObtenida.hostOrigen + " fila-> " + row);
//                }

                //marcamos para la revision
                secuenciasRecibidas.marcaParaRevision(secuenciaObtenida.secuencia);

            } else {
                secuenciasRecibidas.setEsperandoOtraLAN(!secuenciasRecibidas.isEsperandoOtraLAN());
            }
            return esCorrecto;
        }
    }

    private void checkLlegaEnOrden(Secuencia secuenciaObtenida, SecuenciasRecibidas secuenciasRecibidas) {
        boolean enOrden = true;


        if (secuenciaObtenida.secuencia > 0) {
            log.fine("Lan " + (secuenciaObtenida.isLan1 ? "1" : "2") + " Timestamp secuencia " + secuenciaObtenida.secuencia + " : " + secuenciasRecibidas.getTimestampLan(secuenciaObtenida)
                    + " Timestamp secuencia -1: " + secuenciasRecibidas.getTimestampLan(secuenciaObtenida, 1)
                    + " En orden? " + enOrden);

            enOrden = secuenciasRecibidas.getTimestampLan(secuenciaObtenida) > secuenciasRecibidas.getTimestampLan(secuenciaObtenida, 1);
        }
        if (secuenciaObtenida.secuencia > 1 && enOrden) {
            enOrden = secuenciasRecibidas.getTimestampLan(secuenciaObtenida, 1) >= secuenciasRecibidas.getTimestampLan(secuenciaObtenida, 2);

            log.fine("Lan " + (secuenciaObtenida.isLan1 ? "1" : "2") + " Timestamp secuencia -1 " + secuenciaObtenida.secuencia + " : " + secuenciasRecibidas.getTimestampLan(secuenciaObtenida, -1)
                    + " Timestamp secuencia -2: " + secuenciasRecibidas.getTimestampLan(secuenciaObtenida, 2)
                    + " En orden? " + enOrden);

        }


        if (!enOrden) {
            //                            no llega en orden, error
            tableModel.setComprobacion(
                    Configuracion.AVISO + " no ha llegado en orden la secuencia " + secuenciaObtenida.secuencia + " en la "
                    + (secuenciaObtenida.isLan1 ? " LAN 1 GrupoMulticast: " + parGrupoMulticast.GR1 : " LAN 2 GrupoMulticast: " + parGrupoMulticast.GR2)
                    + " origen: " + secuenciaObtenida.hostOrigen, row);
            //no lo consideramos error
            //tableModel.incrementaFalloIncrementaPaquete(row, secuenciaObtenida.isLan1);
            tableModel.incrementaAviso(row);

            log.info(Configuracion.AVISO + " no ha llegado en orden la secuencia " + secuenciaObtenida.secuencia
                    + " en la "
                    + (secuenciaObtenida.isLan1 ? " LAN 1 GrupoMulticast: " + parGrupoMulticast.GR1 : " LAN 2 GrupoMulticast: " + parGrupoMulticast.GR2)
                    + " origen: " + secuenciaObtenida.hostOrigen + " fila-> " + row);

        }


    }

    private long revisamosCola() {


        for (SecuenciasRecibidas secuenciasRecibidas : mapaSecuencias.values()) {


            long[] secuenciasRecibidasLAN1 = secuenciasRecibidas.getSecuenciasRecibidasLAN1();
            long[] secuenciasRecibidasLAN2 = secuenciasRecibidas.getSecuenciasRecibidasLAN2();

            for (int i = 0; i < secuenciasRecibidasLAN1.length; i++) {


                if (secuenciasRecibidas.esMarcadaParaRevision(i)) {


                    long diferencia = Math.abs(secuenciasRecibidasLAN1[i] - secuenciasRecibidasLAN2[i]);

                    if (diferencia > difTiempoSecuenciasMills) {

                        //ERROR, FALTA SECUENCIA
                        if (secuenciasRecibidasLAN1[i] == -1 || secuenciasRecibidasLAN2[i] == -1) {

                            if (secuenciasRecibidas.esMarcadaParaSegundaRevision(i)) {

                                String lanFalta = secuenciasRecibidasLAN1[i] == -1 ? "LAN 1" : "LAN 2";
                                tableModel.setComprobacion(
                                        Configuracion.INCORRECTO + " falta o no ha llegado a tiempo la secuencia " + i + " en la " + lanFalta, row);
//                                        + " Origen Lan1: " + secuenciasRecibidas.getHostOrigenLan1()
//                                        + " Origen Lan2: " + secuenciasRecibidas.getHostOrigenLan2()
//                                        + " GrupoMulticast1: " + parGrupoMulticast.GR1
//                                        + " GrupoMulticast2: " + parGrupoMulticast.GR2, row);
                                
                                
                                tableModel.incrementaFallo(row);


                                log.info(Configuracion.INCORRECTO + " falta o no ha llegado a tiempo la secuencia " + i + " en la " + lanFalta
                                        + " Origen Lan1: " + secuenciasRecibidas.getHostOrigenLan1()
                                        + " Origen Lan2: " + secuenciasRecibidas.getHostOrigenLan2()
                                        + " GrupoMulticast1: " + parGrupoMulticast.GR1
                                        + " GrupoMulticast2: " + parGrupoMulticast.GR2
                                        + " fila-> " + row);

                                log.log(Level.FINE, secuenciasRecibidas.toString());

                                secuenciasRecibidas.limpiaMarcaParaRevision(i);
                                //reinicio contador ya que he mandado mensaje
                                secuenciasRecibidas.reiniciaTimestampALL(-1, i);
                            } else {
                                secuenciasRecibidas.marcaParaSegundaRevision(i);
                            }
                        } else {
                            if (secuenciasRecibidas.esMarcadaParaSegundaRevision(i)) {
                                //ES UN ERROR DE TIMESTAMP
                                boolean isLan1 = secuenciasRecibidasLAN1[i] > secuenciasRecibidasLAN2[i];
                                tableModel.setComprobacion(
                                        Configuracion.INCORRECTO + " ha llegado mas de " + difTiempoSecuenciasMills + " ms tarde la secuencia "
                                        + i + " en la LAN " + (isLan1 ? "1" : "2")
                                        + " Origen Lan1: " + secuenciasRecibidas.getHostOrigenLan1()
                                        + " Origen Lan2: " + secuenciasRecibidas.getHostOrigenLan2()
                                        + " GrupoMulticast1: " + parGrupoMulticast.GR1
                                        + " GrupoMulticast2: " + parGrupoMulticast.GR2, row);
                                tableModel.incrementaFallo(row);

                                log.info(Configuracion.INCORRECTO + " ha llegado mas de " + difTiempoSecuenciasMills + " ms tarde la secuencia "
                                        + i + " en la LAN " + (isLan1 ? "1" : "2")
                                        + " Origen Lan1: " + secuenciasRecibidas.getHostOrigenLan1()
                                        + " Origen Lan2: " + secuenciasRecibidas.getHostOrigenLan2()
                                        + " GrupoMulticast1: " + parGrupoMulticast.GR1
                                        + " GrupoMulticast2: " + parGrupoMulticast.GR2
                                        + " fila-> " + row);

                                secuenciasRecibidas.limpiaMarcaParaRevision(i);
                                //reinicio contador ya que he mandado mensaje
                                secuenciasRecibidas.reiniciaTimestampALL(-1, i);
                            } else {
                                secuenciasRecibidas.marcaParaSegundaRevision(i);
                            }
                        }


                    } else {
                        //ha llegado en orden
                        secuenciasRecibidas.limpiaMarcaParaRevision(i);
                    }

                } else {

                    if (secuenciasRecibidasLAN1[i] != secuenciasRecibidasLAN2[i] && (secuenciasRecibidasLAN1[i] != -1 || secuenciasRecibidasLAN2[i] != -1)) {
                        //si no son iguales
                        //una de las 2 lanes NO tiene secuencia
                        //no ha llegado una de las secuencias
                        if (!secuenciasRecibidas.esMarcadaParaRevision(i)) {
                            secuenciasRecibidas.marcaParaRevision(i);
                        }
                    }
                }
            }
        }

        return Calendar.getInstance().getTimeInMillis();
    }

    /**
     *
     */
    public void limpiaContadores() {
        for (SecuenciasRecibidas secuenciasRecibidas : mapaSecuencias.values()) {
            for (int i = 0; i < SecuenciasRecibidas.MAX_LENGTH; i++) {
                secuenciasRecibidas.reiniciaTimestampALL(-1, i);
            }
        }
    }
}