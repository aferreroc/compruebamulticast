/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ControlPanel.java
 *
 * Created on 25-ene-2012, 16:28:08
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import es.indra.scm.sca.sacta.compruebamulticast.config.Configuracion;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 *
 * @author aferrero
 */
public class ControlPanel extends java.awt.Panel {

    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ControlPanel.class.getName());
    //Threads: McastTableModelC, McastTableModelR
    //MulticastLineaRunnableC, MulticastLineaRunnableR
    private final ExecutorService executor = Executors.newFixedThreadPool(2);
    private McastTableModel mcastTableModelC = null;
    private McastTableModel mcastTableModelR = null;
    private MulticastLineaRunnable mcastLineaRunnableC = null;
    private MulticastLineaRunnable mcastLineaRunnableR = null;

    public ControlPanel(Configuracion conf) {

        //arrancamos aqui los runnables de las tablas para ver los mensajes
        if (!conf.gruposC.isEmpty()) {
            mcastTableModelC = new McastTableModel(true);
            mcastLineaRunnableC = new MulticastLineaRunnable(conf.C1, conf.C2, conf.gruposC.values(), mcastTableModelC,
                    conf.getTiempoRefrescoColaSecs(), conf.getDifTiempoSecuenciasMills());
        }
        if (!conf.gruposR.isEmpty()) {
            mcastTableModelR = new McastTableModel(false);
            mcastLineaRunnableR = new MulticastLineaRunnable(conf.R1, conf.R2, conf.gruposR.values(), mcastTableModelR,
                    conf.getTiempoRefrescoColaSecs(), conf.getDifTiempoSecuenciasMills());
        }



        initComponents();

        //de momento deshabilito el boton de silenciar
        botonSilenciar.setEnabled(false);
        botonSilenciar.setVisible(false);


        //Texto en el borde de los paneles
        TitledBorder borderC = BorderFactory.createTitledBorder(conf.getTitulo("C"));
        borderC.setTitleJustification(TitledBorder.CENTER);
        panelC.setBorder(borderC);

        TitledBorder borderR = BorderFactory.createTitledBorder(conf.getTitulo("R"));
        borderR.setTitleJustification(TitledBorder.CENTER);
        panelR.setBorder(borderR);

        //Nuevos table model para las tablas
        if (mcastTableModelC != null) {
            tablaC.setModel(mcastTableModelC);
        }
        if (mcastTableModelR != null) {
            tablaR.setModel(mcastTableModelR);
        }


        //Mis propios cell renderers y tamanos columnas
        tablaC.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tablaR.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);


        TableCellRenderer centradoCellRenderer = new CentradoCellRenderer();
        TableCellRenderer estadoCellRenderer = new EstadoCellRenderer();
        TableCellRenderer buttonRenderer = new JTableButtonRenderer();


        TableColumn col;
        col = tablaC.getColumnModel().getColumn(0);
        col.setPreferredWidth(120);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaC.getColumnModel().getColumn(1);
        col.setPreferredWidth(120);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaC.getColumnModel().getColumn(2);
        col.setPreferredWidth(60);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaC.getColumnModel().getColumn(3); //Estado
        col.setPreferredWidth(450);
        col.setCellRenderer(estadoCellRenderer);

        col = tablaC.getColumnModel().getColumn(4); // Num
        col.setPreferredWidth(40);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaC.getColumnModel().getColumn(5); //Num
        col.setPreferredWidth(40);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaC.getColumnModel().getColumn(6); //Avisos
        col.setPreferredWidth(40);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaC.getColumnModel().getColumn(7); //Fallos
        col.setPreferredWidth(40);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaC.getColumnModel().getColumn(8); //Porcentaje
        col.setPreferredWidth(50);
        col.setCellRenderer(centradoCellRenderer);
        
        col = tablaC.getColumnModel().getColumn(9); //Fuentas Lan 1
        col.setPreferredWidth(50);
        col.setCellRenderer(centradoCellRenderer);
        
        col = tablaC.getColumnModel().getColumn(10); //Fuentas Lan 2
        col.setPreferredWidth(50);
        col.setCellRenderer(centradoCellRenderer);
        
        col = tablaC.getColumnModel().getColumn(11); //Boton Fuentes
        col.setPreferredWidth(100);
        col.setCellRenderer(buttonRenderer);
        tablaC.addMouseListener(new JTableButtonMouseListener(tablaC));

        //
        // TABLA RADAR
        //

        col = tablaR.getColumnModel().getColumn(0);
        col.setPreferredWidth(120);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaR.getColumnModel().getColumn(1);
        col.setPreferredWidth(120);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaR.getColumnModel().getColumn(2);
        col.setPreferredWidth(60);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaR.getColumnModel().getColumn(3); //Estado
        col.setPreferredWidth(450);
        col.setCellRenderer(estadoCellRenderer);

        col = tablaR.getColumnModel().getColumn(4); // Num
        col.setPreferredWidth(40);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaR.getColumnModel().getColumn(5); //Num
        col.setPreferredWidth(40);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaR.getColumnModel().getColumn(6); //Avisos
        col.setPreferredWidth(40);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaR.getColumnModel().getColumn(7); //Fallos
        col.setPreferredWidth(40);
        col.setCellRenderer(centradoCellRenderer);

        col = tablaR.getColumnModel().getColumn(8); //Porcentaje
        col.setPreferredWidth(50);
        col.setCellRenderer(centradoCellRenderer);
        
        col = tablaR.getColumnModel().getColumn(9); //Fuentas Lan 1
        col.setPreferredWidth(50);
        col.setCellRenderer(centradoCellRenderer);
        
        col = tablaR.getColumnModel().getColumn(10); //Fuentas Lan 2
        col.setPreferredWidth(50);
        col.setCellRenderer(centradoCellRenderer);
        
        col = tablaR.getColumnModel().getColumn(11); //Boton Fuentes
        col.setPreferredWidth(100);
        col.setCellRenderer(buttonRenderer);
        tablaR.addMouseListener(new JTableButtonMouseListener(tablaR));




        botonSilenciar.setText("Silenciar");
        botonSilenciar.setEnabled(false);


        //Control
        if (mcastLineaRunnableC != null) {
            executor.execute(mcastLineaRunnableC);
        }

        //Radar
        if (mcastLineaRunnableR != null) {
            executor.execute(mcastLineaRunnableR);
        }


        botonSilenciar.setEnabled(true);
        lblInfo.setText("Iniciada comprobacion");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelArriba = new javax.swing.JPanel();
        botonSilenciar = new javax.swing.JToggleButton();
        lblInfo = new javax.swing.JLabel();
        botonLimpiaContadores = new javax.swing.JButton();
        panelCentro = new javax.swing.JPanel();
        panelC = new javax.swing.JPanel();
        scrollPaneC = new javax.swing.JScrollPane();
        tablaC = new javax.swing.JTable();
        panelR = new javax.swing.JPanel();
        scrollPaneR = new javax.swing.JScrollPane();
        tablaR = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        panelArriba.setLayout(new java.awt.BorderLayout());

        botonSilenciar.setText("Silenciar");
        botonSilenciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSilenciarActionPerformed(evt);
            }
        });
        panelArriba.add(botonSilenciar, java.awt.BorderLayout.WEST);

        lblInfo.setText("Comprobacion Multicast");
        panelArriba.add(lblInfo, java.awt.BorderLayout.CENTER);

        botonLimpiaContadores.setText("Limpiar Contadores");
        botonLimpiaContadores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLimpiaContadoresActionPerformed(evt);
            }
        });
        panelArriba.add(botonLimpiaContadores, java.awt.BorderLayout.EAST);

        add(panelArriba, java.awt.BorderLayout.NORTH);

        panelCentro.setLayout(new javax.swing.BoxLayout(panelCentro, javax.swing.BoxLayout.PAGE_AXIS));

        panelC.setBorder(javax.swing.BorderFactory.createTitledBorder("Control"));
        panelC.setLayout(new javax.swing.BoxLayout(panelC, javax.swing.BoxLayout.LINE_AXIS));

        tablaC.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Grupo Multicast C1", "Grupo Multicast C2", "Puerto", "Estado", "Fallos", "Num. paquetes", "Porcentaje"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaC.getTableHeader().setReorderingAllowed(false);
        scrollPaneC.setViewportView(tablaC);

        panelC.add(scrollPaneC);

        panelCentro.add(panelC);

        panelR.setBorder(javax.swing.BorderFactory.createTitledBorder("Radar"));
        panelR.setLayout(new javax.swing.BoxLayout(panelR, javax.swing.BoxLayout.LINE_AXIS));

        tablaR.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Grupo Multicast R1", "Grupo Multicast R2", "Puerto", "Estado", "Fallos", "Num. Paquetes", "Porcentaje"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaR.getTableHeader().setReorderingAllowed(false);
        scrollPaneR.setViewportView(tablaR);

        panelR.add(scrollPaneR);

        panelCentro.add(panelR);

        add(panelCentro, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void botonSilenciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSilenciarActionPerformed

        if (botonSilenciar.getText().equals("Silenciar")) {
            lblInfo.setText("Parando");
            stop();
        } else {
            lblInfo.setText("Iniciando");
            start();
        }

    }//GEN-LAST:event_botonSilenciarActionPerformed

    private void botonLimpiaContadoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonLimpiaContadoresActionPerformed

        if (mcastLineaRunnableC != null) {
            mcastLineaRunnableC.limpiaContadores();
        }

        if (mcastTableModelC != null) {
            mcastTableModelC.limpiaContadores();
        }

        //Radar
        if (mcastLineaRunnableR != null) {
            mcastLineaRunnableR.limpiaContadores();
        }

        if (mcastTableModelR != null) {
            mcastTableModelR.limpiaContadores();
        }
    }//GEN-LAST:event_botonLimpiaContadoresActionPerformed

    private void start() {

        botonSilenciar.setText("Silenciar");
        botonSilenciar.setEnabled(false);

        if (mcastLineaRunnableC != null) {
            mcastLineaRunnableC.start();
        }
        if (mcastLineaRunnableR != null) {
            mcastLineaRunnableR.start();
        }

        botonSilenciar.setEnabled(true);
        lblInfo.setText("Iniciada comprobacion");

    }

    private void stop() {

        botonSilenciar.setText("Reanudar");
        botonSilenciar.setEnabled(false);


        if (mcastLineaRunnableC != null) {
            mcastLineaRunnableC.stop();
        }
        if (mcastLineaRunnableR != null) {
            mcastLineaRunnableR.stop();
        }

        //no paramos las tablas... 
        //mcastTableModelC.stop();
        //mcastTableModelR.stop();

        //el executor tampoco lo paramos...
        //if (executor != null) {
        //    try {
        //        executor.awaitTermination(3, TimeUnit.SECONDS);
        //    } catch (InterruptedException ex) {
        //        log.log(Level.INFO, null, ex);
        //    }
        //}


        botonSilenciar.setEnabled(true);
        lblInfo.setText("Parada comprobacion");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonLimpiaContadores;
    private javax.swing.JToggleButton botonSilenciar;
    private javax.swing.JLabel lblInfo;
    private javax.swing.JPanel panelArriba;
    private javax.swing.JPanel panelC;
    private javax.swing.JPanel panelCentro;
    private javax.swing.JPanel panelR;
    private javax.swing.JScrollPane scrollPaneC;
    private javax.swing.JScrollPane scrollPaneR;
    private javax.swing.JTable tablaC;
    private javax.swing.JTable tablaR;
    // End of variables declaration//GEN-END:variables
}
