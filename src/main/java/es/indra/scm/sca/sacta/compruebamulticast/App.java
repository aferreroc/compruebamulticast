package es.indra.scm.sca.sacta.compruebamulticast;

import es.indra.scm.sca.sacta.compruebamulticast.config.Configuracion;
import es.indra.scm.sca.sacta.compruebamulticast.config.ConfiguracionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Hello world!
 *
 */
public class App {

    private static final char[] PASSWORD = "enfldsgbnlsngdlksdsgm".toCharArray();
    private static final byte[] SALT = {
        (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,
        (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,};
    private static final int intentos = 5;
    //private static final String HMCAST_PROP_FILE = ClassLoader.getSystemResource("etc/hmcast.properties").getFile();
    private static final String HMCAST_PROP_FILE = "etc/hmcast.properties";
    private static final String PWD_PROP_NAME = "PASSWORD";

    public static void main(String[] args) {

        //Leo configuracion
        String fileName;
        boolean leerSACTA = false;

        if (args.length == 2 && args[0].trim().toUpperCase().equals("SACTA")) {
            leerSACTA = true;
            fileName = args[1].trim();
        } else if (args.length == 1) {
            fileName = args[0].trim();
        } else {
            System.out.println("Use: java -jar CompruebaMulticast [SACTA] <fichero_configuracion>");
            return;
        }
        System.out.println("Fichero configuracion: " + fileName);


        Properties propPwd = new Properties();
        try {
            File f = new File(HMCAST_PROP_FILE);
            if (f.exists()) {
                propPwd.load(new FileInputStream(f));
            } else {
                propPwd.load(new FileInputStream("/Users/aferrero/Developer/proyectos/INDRA/CompruebaMulticast/target/classes/" + HMCAST_PROP_FILE));
            }
        } catch (IOException ex) {
            //lanzado por prop.load(new FileInputStream(fileName));
            System.err.println("No se ha podido cargar el fichero de propiedades: " + HMCAST_PROP_FILE);
            return;
        }

        try {
            //Muestro password
            if (login(propPwd, intentos)) {

                //continuo

                Properties properties = new Properties();
                properties.load(new FileInputStream(fileName));

                Configuracion conf = new Configuracion(properties, leerSACTA, true);


                //--------------------------------------------------------------------------------------------------------------------------------
                Properties props = System.getProperties();
                props.setProperty("java.net.preferIPv4Stack", "true");
                System.setProperties(props);

                //La ventana
                JFrame jFrame = new JFrame();
                jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                jFrame.setContentPane(new ControlPanel(conf));
                //jFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
                jFrame.setSize(1024, 600);
                jFrame.setVisible(true);
            }

        } catch (IOException ex) {
            //lanzado por prop.load(new FileInputStream(fileName));
            System.err.println("No se ha podido cargar el fichero de configuracion: " + fileName);
        } catch (ConfiguracionException ex) {
            //lanzado por prop.load(new FileInputStream(fileName));
            System.err.println(ex.toString());
        }
    }

    private static boolean login(Properties propPwd, int intentos) {

        String pwdFile = propPwd.getProperty(PWD_PROP_NAME);

        if (intentos <= 0) {
            return false;
        }
        // Components related to "login" field
//        JLabel label_loginname = new JLabel("Enter your login name:");
//        JTextField loginname = new JTextField(15);
        // loginname.setText("EnterLoginNameHere"); // Pre-set some text

        // Components related to "password" field
        JLabel label_password = new JLabel("Clave de acceso:");
        JPasswordField password = new JPasswordField();
        // password.setEchoChar('@'); // Sets @ as masking character
        // password.setEchoChar('\000'); // Turns off masking


        Object[] array = {label_password, password};
        //int res = JOptionPane.showConfirmDialog(null, array, "Login",JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE);

        String[] options = {"Aceptar", "Cancelar", "Cambio clave"};
        int res = JOptionPane.showOptionDialog(null, array, "HMCAST - Clave de acceso",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

        // User hit OK
        if (res == JOptionPane.YES_OPTION || res == JOptionPane.CANCEL_OPTION) {
            String pwEncrypt;
            try {
                pwEncrypt = encrypt(new String(password.getPassword()));
            } catch (GeneralSecurityException ex) {
                //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }

//            System.out.println(pwEncrypt);
//            System.out.println(pwdFile);
            if (pwEncrypt.equals(pwdFile)) {
                if (res == JOptionPane.YES_OPTION) {
                    return true;
                } else {
                    return cambioPassword(propPwd);
                }
            } else {
                return login(propPwd, intentos--);
            }
        }

//        // User hit CANCEL
//        if (res == JOptionPane.CANCEL_OPTION) {
//            System.out.println("CANCEL_OPTION");
//        }
//
//        // User closed the window without hitting any button
//        if (res == JOptionPane.CLOSED_OPTION) {
//            System.out.println("CLOSED_OPTION");
//        }

        return false;
    }

    private static boolean cambioPassword(Properties propPwd) {
        JLabel label_password = new JLabel("Clave de acceso:");
        JPasswordField password = new JPasswordField();

        JLabel label_password2 = new JLabel("Repita clave de acceso:");
        JPasswordField password2 = new JPasswordField();


        Object[] array = {label_password, password, label_password2, password2};
        //int res = JOptionPane.showConfirmDialog(null, array, "Login",JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE);

        String[] options = {"Aceptar", "Cancelar"};
        int res = JOptionPane.showOptionDialog(null, array, "HMCAST - Cambio clave de acceso",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

        // User hit OK
        if (res == JOptionPane.YES_OPTION) {
            String pwEncrypt;
            String pwEncrypt2;
            try {
                pwEncrypt = encrypt(new String(password.getPassword()));
                pwEncrypt2 = encrypt(new String(password.getPassword()));
            } catch (GeneralSecurityException ex) {
                //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }

//            System.out.println(pwEncrypt);
//            System.out.println(pwdFile);
            if (pwEncrypt.equals(pwEncrypt2)) {
                propPwd.setProperty(PWD_PROP_NAME, pwEncrypt2);
                try {
                    propPwd.store(new FileOutputStream(HMCAST_PROP_FILE), null);
                    return true;
                } catch (IOException ex) {
                    return false;
                }
            } else {
                return cambioPassword(propPwd);
            }
        }

        return false;
    }

    private static String encrypt(String property) throws GeneralSecurityException {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
        pbeCipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
        return base64Encode(pbeCipher.doFinal(property.getBytes()));
    }

    private static String base64Encode(byte[] bytes) {
        // NB: This class is internal, and you probably should use another impl
        return new BASE64Encoder().encode(bytes);
    }

    private static String decrypt(String property) throws GeneralSecurityException, IOException {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
        pbeCipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
        return new String(pbeCipher.doFinal(base64Decode(property)));
    }

    private static byte[] base64Decode(String property) throws IOException {
        // NB: This class is internal, and you probably should use another impl
        return new BASE64Decoder().decodeBuffer(property);
    }
}
