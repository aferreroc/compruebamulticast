/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author aferrero
 */
public class ParGrupoMulticast {

    public String GR1 = null;
    public String GR2 = null;
    public int puerto = 0;
    public String estado = null;
    private final Set<String> fuentesLan1 = new HashSet<String>();
    private final Set<String> fuentesLan2 = new HashSet<String>();

    private ParGrupoMulticast() {
    }

    public ParGrupoMulticast(String grupo1, String grupo2, int puerto) {
        GR1 = grupo1;
        GR2 = grupo2;
        this.puerto = puerto;
    }

    public void setAll(String grupo1, String grupo2, int puerto) {
        GR1 = grupo1;
        GR2 = grupo2;
        this.puerto = puerto;
    }

    /**
     * @return the fuentesLan1
     */
    public Set<String> getFuentesLan1() {
        return fuentesLan1;
    }

    /**
     * @return the fuentesLan2
     */
    public Set<String> getFuentesLan2() {
        return fuentesLan2;
    }

    public void addFuenteLan(boolean isLan1, String fuente) {
        if (isLan1) {
            fuentesLan1.add(fuente);
        } else {
            fuentesLan2.add(fuente);
        }
    }
    
    public void limpiaFuentesLan() {
        fuentesLan1.clear();
        fuentesLan2.clear();
    }
    
    @Override
    public String toString() {
        return GR1 + " " + GR2 + " " + puerto;
    }
 }
