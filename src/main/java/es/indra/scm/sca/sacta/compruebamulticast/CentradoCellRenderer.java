/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author aferrero
 */
public class CentradoCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        setEnabled(table == null || table.isEnabled()); // see question above 

//        if ((row % 2) == 0) {
//            setBackground(Color.red);
//        } else {
//            setBackground(null);
//        }
       
        setHorizontalAlignment(SwingConstants.CENTER);

        super.getTableCellRendererComponent(table, value, selected, focused, row, column);

        return this;
    }
}
