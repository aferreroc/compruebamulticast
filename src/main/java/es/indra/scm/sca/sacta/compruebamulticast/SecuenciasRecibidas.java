/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

/**
 *
 * @author aferrero
 */
public final class SecuenciasRecibidas {

    public static final int MAX_LENGTH = 290;
    private long[] secuenciasRecibidasLAN1;
    private long[] secuenciasRecibidasLAN2;
    private long lastTimestampLAN1, lastTimestampLAN2;
    private boolean esperandoOtraLAN = true;
    private int[] marcadasParaRevision; //usamos esto para revisar que llegan correctamente los paquetes
    private String hostOrigenLan1, hostOrigenLan2;

    public SecuenciasRecibidas(long timestamp, Secuencia secuenciaObtenida) {
        secuenciasRecibidasLAN1 = new long[MAX_LENGTH];
        secuenciasRecibidasLAN2 = new long[MAX_LENGTH];
        marcadasParaRevision = new int[MAX_LENGTH];
        for (int i = 0; i < MAX_LENGTH; i++) {
            secuenciasRecibidasLAN1[i] = -1L;
            secuenciasRecibidasLAN2[i] = -1L;
            marcadasParaRevision[i] = 0;
        }
        lastTimestampLAN1 = -1L;
        lastTimestampLAN2 = -1L;

        if (secuenciaObtenida.isLan1) {
            secuenciasRecibidasLAN1[secuenciaObtenida.secuencia] = timestamp;
            lastTimestampLAN1 = timestamp;

            // y de la otra lan, si es -1 ponemos lo mismo
            if (lastTimestampLAN2 == -1L) {
                lastTimestampLAN2 = timestamp;
            }
            hostOrigenLan1 = secuenciaObtenida.hostOrigen;
        } else {
            secuenciasRecibidasLAN2[secuenciaObtenida.secuencia] = timestamp;
            lastTimestampLAN2 = timestamp;

            // y de la otra lan, si es -1 ponemos lo mismo
            if (lastTimestampLAN1 == -1L) {
                lastTimestampLAN1 = timestamp;
            }
            hostOrigenLan2 = secuenciaObtenida.hostOrigen;
        }

    }

    public void reiniciaTimestampALL(long timestamp, int secuencia) {
        secuenciasRecibidasLAN1[secuencia] = timestamp;
        secuenciasRecibidasLAN2[secuencia] = timestamp;
        marcadasParaRevision[secuencia] = 0;
    }

    public void setTimestamp(long timestamp, Secuencia secuenciaObtenida) {
        if (secuenciaObtenida.isLan1) {
            secuenciasRecibidasLAN1[secuenciaObtenida.secuencia] = timestamp;
            lastTimestampLAN1 = timestamp;

            // y de la otra lan, si es -1 ponemos lo mismo
            if (lastTimestampLAN2 == -1L) {
                lastTimestampLAN2 = timestamp;
            }
        } else {
            secuenciasRecibidasLAN2[secuenciaObtenida.secuencia] = timestamp;
            lastTimestampLAN2 = timestamp;

            // y de la otra lan, si es -1 ponemos lo mismo
            if (lastTimestampLAN1 == -1L) {
                lastTimestampLAN1 = timestamp;
            }
        }
    }

    public long getTimestampLan(Secuencia secuenciaObtenida) {
        if (secuenciaObtenida.isLan1) {
            return secuenciasRecibidasLAN1[secuenciaObtenida.secuencia];
        } else {
            return secuenciasRecibidasLAN2[secuenciaObtenida.secuencia];
        }
    }

    public long getTimestampOtherLan(Secuencia secuenciaObtenida) {
        if (secuenciaObtenida.isLan1) {
            return secuenciasRecibidasLAN2[secuenciaObtenida.secuencia];
        } else {
            return secuenciasRecibidasLAN1[secuenciaObtenida.secuencia];
        }
    }

    public long getTimestampLan(Secuencia secuenciaObtenida, int restaSecuencia) {
        
        int secuencia = secuenciaObtenida.secuencia - restaSecuencia;
        if (secuencia < 0 || secuencia >= secuenciasRecibidasLAN1.length) {
            return -1;
        }
        if (secuenciaObtenida.isLan1) {
            return secuenciasRecibidasLAN1[secuencia];
        } else {
            return secuenciasRecibidasLAN2[secuencia];
        }
    }

    public long getTimestampOtherLan(Secuencia secuenciaObtenida, int restaSecuencia) {
        int secuencia = secuenciaObtenida.secuencia - restaSecuencia;
        if (secuencia < 0 || secuencia >= secuenciasRecibidasLAN1.length) {
            return -1;
        }
        if (secuenciaObtenida.isLan1) {
            return secuenciasRecibidasLAN2[secuencia];
        } else {
            return secuenciasRecibidasLAN1[secuencia];
        }
    }

    /**
     * @return the secuenciasRecibidasLAN1
     */
    public long[] getSecuenciasRecibidasLAN1() {
        return secuenciasRecibidasLAN1;
    }

    /**
     * @return the secuenciasRecibidasLAN2
     */
    public long[] getSecuenciasRecibidasLAN2() {
        return secuenciasRecibidasLAN2;
    }

    public long getLastTimestampLan(Secuencia secuenciaObtenida) {
        if (secuenciaObtenida.isLan1) {
            return lastTimestampLAN1;
        } else {
            return lastTimestampLAN2;
        }
    }

    public long getLastTimestampOtherLan(Secuencia secuenciaObtenida) {
        if (secuenciaObtenida.isLan1) {
            return lastTimestampLAN2;
        } else {
            return lastTimestampLAN1;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb1 = new StringBuilder("Lan1: ");
        StringBuilder sb2 = new StringBuilder("Lan2: ");
        StringBuilder sb3 = new StringBuilder("Reviion: ");
        for (int i = 0; i < secuenciasRecibidasLAN1.length; i++) {
            sb1.append(i).append("->").append(secuenciasRecibidasLAN1[i]).append(" ");
            sb2.append(i).append("->").append(secuenciasRecibidasLAN2[i]).append(" ");
            sb3.append(i).append("->").append(marcadasParaRevision[i]).append(" ");
        }

        sb1.append("\n").append(sb2).append("\n").append(sb3);
        sb1.append("\nLast timestamp Lan1: ").append(lastTimestampLAN1);
        sb1.append("\nLast timestapn Lan2: ").append(lastTimestampLAN2);
        return sb1.toString();
    }

    /**
     * @return the esperandoOtraLAN
     */
    public boolean isEsperandoOtraLAN() {
        return esperandoOtraLAN;
    }

    /**
     * @param esperandoOtraLAN the esperandoOtraLAN to set
     */
    public void setEsperandoOtraLAN(boolean esperandoOtraLAN) {
        this.esperandoOtraLAN = esperandoOtraLAN;
    }

    public boolean esMarcadaParaRevision(int secuencia) {
        return marcadasParaRevision[secuencia] > 0;
    }

    public boolean esMarcadaParaSegundaRevision(int secuencia) {
        return marcadasParaRevision[secuencia] == 2;
    }

    public void marcaParaRevision(int secuencia) {
        this.marcadasParaRevision[secuencia] = 1;
    }

    public void marcaParaSegundaRevision(int secuencia) {
        this.marcadasParaRevision[secuencia] = 2;
    }

    public void limpiaMarcaParaRevision(int secuencia) {
        this.marcadasParaRevision[secuencia] = 0;
    }

    /**
     * @return the hostOrigenLan1
     */
    public String getHostOrigenLan1() {
        return hostOrigenLan1;
    }

    /**
     * @return the hostOrigenLan2
     */
    public String getHostOrigenLan2() {
        return hostOrigenLan2;
    }
}