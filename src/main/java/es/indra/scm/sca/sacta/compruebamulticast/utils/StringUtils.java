/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.utils;

/**
 *
 * @author aferrero
 */
public class StringUtils {

    public static int getIntegerValue(String strValue, int defalutValue) {
        try {
            return Integer.valueOf(strValue).intValue();
        } catch (NumberFormatException e) {
            return defalutValue;
        }
    }
    
    public static boolean isEmpty(String value) {
        return value.trim().length()==0;
    }
}
