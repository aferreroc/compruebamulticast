/*******************************************************************************
 * Name      : es.indra.scm.sacta.config.IDomain
 * Project   : [SC|SGU|PSI|...]
 * Subproject: spv-alm
 * CSCI      :
 * CSC       :
 *******************************************************************************
 * Last change info.
 *******************************************************************************
 * #$Author: dmoya $
 * #$Date: 2011-09-16 11:59:51 +0200 (vie, 16 sep 2011) $
 * #$Revision: 51341 $
 *******************************************************************************
 */

package es.indra.scm.sca.sacta.compruebamulticast.config.network;


/**
 * The domain defines the first octet of the ip of all the machines
 * that are part of the domain.
 *
 * @author dmoya
 */
public interface IDomain extends IIdentificableObject, IXMLObjectWriter {

	/**
	 * Assigned a identifier of domain.
	 * @param id String ID.
	 */
	void setId(String id);

	/**
	 * Returns the Name of the domain.
	 * @return String ID.
	 */
	String getName();

	/**
	 * Assigned a Name of domain.
	 * @param name String Name.
	 */
	void setName(String name);

	/**
	 * Returns the first octet of the ip of the machines
	 * that are part of this domain.
	 *
	 * @return String with the first octet of the IP.
	 */
	String getIPPart();

	/**
	 * Assigning the first octet of the ip of the machines that are part of this domain.
	 * @param ipPart IP Part.
	 */
	void setIPPart(String ipPart);

	/**
	 * Add the name of the object center.
	 * @param centre Centre
	 */
	void add(Centre centre);

	/**
	 * Gets center by identifier.
	 * @param centerId Center ID.
	 * @return  Centre
	 */
	ICentre getCentre(String centerId);

	/**
	 * Gets center by ipPart, first and second octet.
	 * @param ipPart IP Part.
	 * @return Centre.
	 */
	ICentre getCentreByIPPart(String ipPart);
}
