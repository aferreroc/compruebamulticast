/**
 * *****************************************************************************
 * Name : es.indra.scm.sacta.config.Domain Project : [SC|SGU|PSI|...]
 * Subproject: spv-alm CSCI : CSC :
 * ******************************************************************************
 * Last change info.
 * ******************************************************************************
 * #$Author: dmoya $ #$Date: 2011-09-16 11:59:51 +0200 (vie, 16 sep 2011) $
 * #$Revision: 51341 $
 * ******************************************************************************
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Class
 * <code>Domain</code> implements
 * <code>IDomain</code>. Defines the parameters of the domain.
 */
public class Domain extends IdentificableObject implements IDomain {

    /**
     *      */
    private static final long serialVersionUID = 1L;
    /**
     * String Name.
     */
    private String name;
    /**
     * String IP Part.
     */
    private String ipPart;
    /**
     * Centres List.
     */
    private HashMap<String, Centre> centres = new HashMap<String, Centre>();

    @Override
    public void setId(String id) {
        super.setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public void add(Centre centre) {
        centres.put(centre.getName(), centre);
    }

    
    public String getIPPart() {
        return ipPart;
    }

    
    public void setIPPart(String ipPart) {
        this.ipPart = ipPart;
    }

    
    public ICentre getCentre(String centerId) {
        return centres.get(centerId);
    }

    
    public ICentre getCentreByIPPart(String ipPart) {
        // TODO hacerlo mas fino y elegante :P

        ICentre ret = null;

        if (ipPart != null) {
            for (ICentre centre : centres.values()) {

                for (IPPartsPair ipPair : centre.getIPPartsPairs()) {
                    if (ipPart.equals(ipPair.getMulticastIPPart()) || ipPart.equals(ipPair.getUnicastIPPart())) {
                        ret = centre;
                        break;
                    }
                }

            }
        }

        return ret;
    }

    
    public void toXML(PrintWriter out) {
        out.println("<domain " + XMLATTR_ID + "=\"" + getId() + "\" name=\"" + name + "\" ipPart=\"" + ipPart + "\">");

        for (ICentre centre : centres.values()) {
            centre.toXML(out);
        }

        out.println("</domain>");
    }
}
