/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author aferrero
 */
public class MensajeSACTA {

    private String sourceDomain;
    private String sourceCentre;
    private String sourceCSCI;
    private String destinationDomain;
    private String destinationCentre;
    private String destinationCSCI;
    private String session;
    private String messageType;
    private String option;
    private String sequence;
    private String length;
    private String time;

    public MensajeSACTA(byte[] buffer) {


        String cabeceraSacta = Hex.encodeHexString(buffer);

        sourceDomain = Integer.toHexString(buffer[0] & 0xFF);
        sourceCentre = Integer.toHexString(buffer[1] & 0xFF);
        sourceCSCI = Integer.toHexString(buffer[2] & 0xFF)
                + Integer.toHexString(buffer[3] & 0xFF);

        destinationDomain = Integer.toHexString(buffer[4] & 0xFF);
        destinationCentre = Integer.toHexString(buffer[5] & 0xFF);
        destinationCSCI = Integer.toHexString(buffer[6] & 0xFF)
                + Integer.toHexString(buffer[7] & 0xFF);
        session = Integer.toHexString(buffer[8] & 0xFF)
                + Integer.toHexString(buffer[9] & 0xFF);
        messageType = Integer.toHexString(buffer[10] & 0xFF)
                + Integer.toHexString(buffer[11] & 0xFF);


        String byte12 = Integer.toHexString(buffer[12] & 0xFF);
        if (byte12.length() >= 2) {
            option = byte12.substring(0, 1);
            sequence = byte12.substring(2) + Integer.toHexString(buffer[13] & 0xFF);
        } else {
            option = "0";
            sequence = byte12 + Integer.toHexString(buffer[13] & 0xFF);
        }



        length = Integer.toHexString(buffer[14] & 0xFF) + Integer.toHexString(buffer[15] & 0xFF);
        time = Integer.toHexString(buffer[16] & 0xFF) + Integer.toHexString(buffer[17] & 0xFF) + Integer.toHexString(buffer[18] & 0xFF) + Integer.toHexString(buffer[19] & 0xFF);




    }

    /**
     * @return the sourceDomain
     */
    public String getSourceDomain() {
        return sourceDomain;
    }

    /**
     * @param sourceDomain the sourceDomain to set
     */
    public void setSourceDomain(String sourceDomain) {
        this.sourceDomain = sourceDomain;
    }

    /**
     * @return the sourceCentre
     */
    public String getSourceCentre() {
        return sourceCentre;
    }

    /**
     * @param sourceCentre the sourceCentre to set
     */
    public void setSourceCentre(String sourceCentre) {
        this.sourceCentre = sourceCentre;
    }

    /**
     * @return the sourceCSCI
     */
    public String getSourceCSCI() {
        return sourceCSCI;
    }

    /**
     * @param sourceCSCI the sourceCSCI to set
     */
    public void setSourceCSCI(String sourceCSCI) {
        this.sourceCSCI = sourceCSCI;
    }

    /**
     * @return the destinationDomain
     */
    public String getDestinationDomain() {
        return destinationDomain;
    }

    /**
     * @param destinationDomain the destinationDomain to set
     */
    public void setDestinationDomain(String destinationDomain) {
        this.destinationDomain = destinationDomain;
    }

    /**
     * @return the destinationCentre
     */
    public String getDestinationCentre() {
        return destinationCentre;
    }

    /**
     * @param destinationCentre the destinationCentre to set
     */
    public void setDestinationCentre(String destinationCentre) {
        this.destinationCentre = destinationCentre;
    }

    /**
     * @return the destinationCSCI
     */
    public String getDestinationCSCI() {
        return destinationCSCI;
    }

    /**
     * @param destinationCSCI the destinationCSCI to set
     */
    public void setDestinationCSCI(String destinationCSCI) {
        this.destinationCSCI = destinationCSCI;
    }

    /**
     * @return the session
     */
    public String getSession() {
        return session;
    }

    /**
     * @param session the session to set
     */
    public void setSession(String session) {
        this.session = session;
    }

    /**
     * @return the messageType
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * @param messageType the messageType to set
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * @return the option
     */
    public String getOption() {
        return option;
    }

    /**
     * @param option the option to set
     */
    public void setOption(String option) {
        this.option = option;
    }

    /**
     * @return the sequence
     */
    public String getSequence() {
        return sequence;
    }

    public int getSequenceIntValue() {
        try {
            return Integer.parseInt(sequence, 16);
        } catch (Exception e) {
            return Integer.MAX_VALUE;
        }
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    /**
     * @return the length
     */
    public String getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(String length) {
        this.length = length;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("sourceDomain: ").append(sourceDomain);
        sb.append(" sourceCentre: ").append(sourceCentre);
        sb.append(" sourceCSCI: ").append(sourceCSCI);
        sb.append(" destinationDomain: ").append(destinationDomain);
        sb.append(" destinationCentre: ").append(destinationCentre);
        sb.append(" destinationCSCI: ").append(destinationCSCI);
        sb.append(" session: ").append(session);
        sb.append(" messageType: ").append(messageType);
        sb.append(" option: ").append(option);
        sb.append(" sequence: ").append(sequence);
        sb.append(" length: ").append(length);
        sb.append(" time: ").append(time);

        return sb.toString();
    }
}
