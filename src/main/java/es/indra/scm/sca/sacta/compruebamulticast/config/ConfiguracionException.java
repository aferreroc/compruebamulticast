/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config;

/**
 *
 * @author aferrero
 */
public class ConfiguracionException extends Exception {
    private static final long serialVersionUID = 1L;

    public ConfiguracionException(String message) {
        super("El fichero de configuracion es erroneo causa: " + message);
    }
    
    
    
}
