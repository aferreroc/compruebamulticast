/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config;

import es.indra.scm.sca.sacta.compruebamulticast.ParGrupoMulticast;
import es.indra.scm.sca.sacta.compruebamulticast.config.network.*;
import java.awt.Color;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author aferrero
 */
public class Configuracion {

    private static final Logger log = Logger.getLogger(Configuracion.class.getName());
    public static final String CORRECTO = "Correcto";
    public static final Color CORRECTO_COLOR = Color.green;
    public static final String AVISO = "Aviso";
    public static final Color AVISO_COLOR = Color.yellow;
    public static final String INCORRECTO = "Incorrecto";
    public static final Color INCORRECTO_COLOR = Color.red;
    public static final String FALTACABECERAS = "Faltan cabeceras";
    public static final Color FALTACABECERAS_COLOR = Color.yellow;
    public static final String FALLO = "Fallo";
    public static final Color FALLO_COLOR = Color.orange;
    public static final String INICIANDO = "Iniciando";
    public static final Color INICIANDO_COLOR = Color.white;
    public static final String INTERRUMPIDO = "Interrumpido";
    public static final Color INTERRUMPIDO_COLOR = null;
    private static final String _C1 = "C1";
    private static final String _C2 = "C2";
    private static final String _R1 = "R1";
    private static final String _R2 = "R2";
    private static final String _C_MCAST = "C_MCAST";
    private static final String _R_MCAST = "R_MCAST";
    private static final String _TIEMPO_REFRESCO_COLA_SECS = "TIEMPO_REFRESCO_COLA_SECS";
    private static final String _DIF_TIEMPO_SECUENCIAS_MILLS = "DIF_TIEMPO_SECUENCIAS_MILLS";
    private static final String _SACTA_PATH = "SACTA_PATH";
    private static final String _NODO = "NODO";
    //
    public static String SACTA_PATH = null;
    public static String NODO = null;
    //
    private int tiempoRefrescoColaSecs = -1;
    private long difTiempoSecuenciasMills = -1L;
    //
    public String C1 = null;
    public String C2 = null;
    public String R1 = null;
    public String R2 = null;
    public InetAddress iAddrC1;
    public InetAddress iAddrC2;
    public InetAddress iAddrR1;
    public InetAddress iAddrR2;
    public Map<String, ParGrupoMulticast> gruposC = new TreeMap<String, ParGrupoMulticast>();
    public Map<String, ParGrupoMulticast> gruposR = new TreeMap<String, ParGrupoMulticast>();
    //
    private static final Pattern ipPattern = Pattern.compile("(\\d{1,3})\\.\\d{1,3}(\\.\\d{1,3}\\.\\d{1,3})");

    public Configuracion(Properties properties, boolean leeSACTA, boolean compruebaValores) throws ConfiguracionException {

        //for (String key : properties.stringPropertyNames()) {
        Enumeration e = properties.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = properties.getProperty(key);

            //System.out.println(key + " => " + value);

            if (key.equals(_NODO)) {
                if (NODO == null) {
                    NODO = value;
                } else {
                    throw new ConfiguracionException("Existe más de una propiedad " + _NODO);
                }
            }

            if (key.equals(_SACTA_PATH)) {
                if (SACTA_PATH == null) {
                    SACTA_PATH = value;
                } else {
                    throw new ConfiguracionException("Existe más de una propiedad " + _SACTA_PATH);
                }
            }


            if (key.equals(_TIEMPO_REFRESCO_COLA_SECS)) {
                if (tiempoRefrescoColaSecs == -1) {
                    try {
                        tiempoRefrescoColaSecs = Integer.valueOf(value).intValue();
                    } catch (NumberFormatException ex) {
                        throw new ConfiguracionException("Valor no numerico en la propiedad " + _TIEMPO_REFRESCO_COLA_SECS);
                    }
                } else {
                    throw new ConfiguracionException("Existe más de una propiedad " + _TIEMPO_REFRESCO_COLA_SECS);
                }
            }

            if (key.equals(_DIF_TIEMPO_SECUENCIAS_MILLS)) {
                if (difTiempoSecuenciasMills == -1) {
                    try {
                        difTiempoSecuenciasMills = Long.valueOf(value).longValue();
                    } catch (NumberFormatException ex) {
                        throw new ConfiguracionException("Valor no numerico en la propiedad " + _DIF_TIEMPO_SECUENCIAS_MILLS);
                    }
                } else {
                    throw new ConfiguracionException("Existe más de una propiedad " + _DIF_TIEMPO_SECUENCIAS_MILLS);
                }
            }

            if (!leeSACTA) {
                if (key.equals(_C1)) {
                    if (C1 == null) {
                        C1 = value;
                        try {
                            iAddrC1 = NetworkInterface.getByName(C1).getInetAddresses().nextElement();
                        } catch (SocketException ex) {
                            log.log(Level.WARNING, null, ex);
                        } catch (NullPointerException ex) {
                            throw new ConfiguracionException("Error en los interfaces de red, revise el interfaz " + C1 + " en la propiedad " + _C1 + " del fichero de configuracion");
                        }
                    } else {
                        throw new ConfiguracionException("Existe mas de una propiedad " + _C1);
                    }
                }
                if (key.equals(_C2)) {
                    if (C2 == null) {
                        C2 = value;
                        try {
                            iAddrC2 = NetworkInterface.getByName(C2).getInetAddresses().nextElement();
                        } catch (SocketException ex) {
                            log.log(Level.WARNING, null, ex);
                        } catch (NullPointerException ex) {
                            throw new ConfiguracionException("Error en los interfaces de red, revise el interfaz " + C2 + " en la propiedad " + _C2 + " del fichero de configuracion");
                        }
                    } else {
                        throw new ConfiguracionException("Existe mas de una propiedad " + _C2);
                    }
                }
                if (key.equals(_R1)) {
                    if (R1 == null) {
                        R1 = value;
                        try {
                            iAddrR1 = NetworkInterface.getByName(R1).getInetAddresses().nextElement();
                        } catch (SocketException ex) {
                            log.log(Level.WARNING, null, ex);
                        } catch (NullPointerException ex) {
                            throw new ConfiguracionException("Error en los interfaces de red, revise el interfaz " + R1 + " en la propiedad " + _R1 + " del fichero de configuracion");
                        }
                    } else {
                        throw new ConfiguracionException("Existe mas de una propiedad " + _R1);
                    }
                }
                if (key.equals(_R2)) {
                    if (R2 == null) {
                        R2 = value;
                        try {
                            iAddrR2 = NetworkInterface.getByName(R2).getInetAddresses().nextElement();
                        } catch (SocketException ex) {
                            log.log(Level.WARNING, null, ex);
                        } catch (NullPointerException ex) {
                            throw new ConfiguracionException("Error en los interfaces de red, revise el interfaz " + R2 + " en la propiedad " + _R2 + " del fichero de configuracion");
                        }
                    } else {
                        throw new ConfiguracionException("Existe mas de una propiedad " + _R2);
                    }
                }

                if (key.startsWith(_C_MCAST)) {
                    appendFromProperties(key, value, gruposC);
                }

                if (key.startsWith(_R_MCAST)) {
                    appendFromProperties(key, value, gruposR);
                }


            }
        }

        //pSystem.out.println("GruposC: " + gruposC.size());
        //System.out.println("GruposR: " + gruposR.size());

        if (leeSACTA) {
            leeConfiguracionSacta();
        }

        if (compruebaValores) {
            compruebaValores();
        }

    }

    private void leeConfiguracionSacta() {
//        System.out.println("Etiqueta: " + SACTAEnvironment.getInstance().getMyNodeId());
//        System.out.println("CENTRO: " + SACTAEnvironment.getInstance().getMyCentreId());
//        System.out.println("CENTRO_LAN2: " + SACTAEnvironment.getInstance().getMyCentreIdLan2());
//        System.out.println("DOMINIO: " + SACTAEnvironment.getInstance().getMyDomainId());
//        System.out.println("ficheroCENTRO: " + SACTAEnvironment.getInstance().ficheroCENTRO);
//        System.out.println("ficheroCENTROLAN2: " + SACTAEnvironment.getInstance().ficheroCENTROLAN2);



        if (SACTAEnvironment.getInstance().getNetworkDomainConfigContext() != null) {

            for (MulticastServerConfig mcConf : NetworkDomainUtil.getMulticastConfig(
                    SACTAEnvironment.getInstance().getNetworkDomainConfigContext(),
                    SACTAEnvironment.getInstance().getMyDomainId(),
                    SACTAEnvironment.getInstance().getMyCentreId(),
                    SACTAEnvironment.getInstance().getMyNodeId(),
                    "udp")) {

                appendFromSACTA(mcConf, gruposC);


                if (C1 == null && mcConf.getLan() == 1) { // && !mcConf.getAddr().isLoopbackAddress()) {
                    try {
                        iAddrC1 = !mcConf.getAddr().isLoopbackAddress() ? mcConf.getAddr() : null;
                        C1 = NetworkInterface.getByInetAddress(iAddrC1).getDisplayName();
                    } catch (Exception ex) {
                        C1 = "";
                    }
                }
                if (C2 == null && mcConf.getLan() == 2) { // && !mcConf.getAddr().isLoopbackAddress()) {
                    try {

                        iAddrC2 = !mcConf.getAddr().isLoopbackAddress() ? mcConf.getAddr() : null;
                        C2 = NetworkInterface.getByInetAddress(iAddrC2).getDisplayName();
                    } catch (Exception ex) {
                        C2 = "";
                    }
                }
            }

        }



        if (SACTAEnvironment.getInstance().getNetworkDomainConfigContextLan2() != null) {

            for (MulticastServerConfig mcConf : NetworkDomainUtil.getMulticastConfig(
                    SACTAEnvironment.getInstance().getNetworkDomainConfigContextLan2(),
                    SACTAEnvironment.getInstance().getMyDomainId(),
                    SACTAEnvironment.getInstance().getMyCentreIdLan2(),
                    SACTAEnvironment.getInstance().getMyNodeId(),
                    "udp")) {

                appendFromSACTA(mcConf, gruposR);


                if (R1 == null && mcConf.getLan() == 1) { // && !mcConf.getAddr().isLoopbackAddress()) {
                    try {

                        iAddrR1 = !mcConf.getAddr().isLoopbackAddress() ? mcConf.getAddr() : null;
                        R1 = NetworkInterface.getByInetAddress(iAddrR1).getDisplayName();
                    } catch (Exception ex) {
                        R1 = "";
                    }
                }
                if (R2 == null && mcConf.getLan() == 2) { // && !mcConf.getAddr().isLoopbackAddress()) {
                    try {
                        iAddrR2 = !mcConf.getAddr().isLoopbackAddress() ? mcConf.getAddr() : null;
                        R2 = NetworkInterface.getByInetAddress(iAddrR2).getDisplayName();
                    } catch (Exception ex) {
                        R2 = "";
                    }
                }
            }

        }

    }

    private void appendFromSACTA(MulticastServerConfig mcConf, Map<String, ParGrupoMulticast> mapLAN) {

        String ipGrupo1 = mcConf.getLan() == 1 && !mcConf.getAddr().isLoopbackAddress() ? mcConf.getMulticastIP() : null;
        String ipGrupo2 = mcConf.getLan() == 2 && !mcConf.getAddr().isLoopbackAddress() ? mcConf.getMulticastIP() : null;

        if (ipGrupo1 == null && ipGrupo2 == null) {
            return;
        }

        String grupoCorto;
        if (ipGrupo1 != null) {
            grupoCorto = dameGrupoCorto(ipGrupo1);
        } else {
            grupoCorto = dameGrupoCorto(ipGrupo2);
        }

        ParGrupoMulticast par = mapLAN.get(grupoCorto);

        if (par == null) {
            par = new ParGrupoMulticast(ipGrupo1, ipGrupo2, mcConf.getPort());
            mapLAN.put(grupoCorto, par);
        } else {
            if (ipGrupo1 != null) {
                par.GR1 = ipGrupo1;
            }
            if (ipGrupo2 != null) {
                par.GR2 = ipGrupo2;
            }
        }

    }

    /**
     *
     * @param key
     * @param value
     * @param lista
     * @throws ConfiguracionException
     */
    private void appendFromProperties(String key, String value, Map<String, ParGrupoMulticast> mapLAN) throws ConfiguracionException {
        if (value.trim().length() == 0) {
            return;
        }

        //parseamos str
        String[] strSplitted = value.split(";");
        if (strSplitted.length == 3) {

            try {
                String grupo1 = strSplitted[0].trim();
                String grupo2 = strSplitted[1].trim();
                String strPuerto = strSplitted[2].trim();
                int puerto = Integer.parseInt(strPuerto);

                String grupoCorto = dameGrupoCorto(grupo1);

                //System.out.println(grupoCorto);

                ParGrupoMulticast par = mapLAN.get(grupoCorto);
                if (par == null) {
                    par = new ParGrupoMulticast(grupo1, grupo2, puerto);
                    mapLAN.put(grupoCorto, par);
                } else {
                    par.setAll(grupo1, grupo2, puerto);
                }



            } catch (NumberFormatException ex) {
                throw new ConfiguracionException("Puerto incorrecto en uno de los valores de " + key);
            }



        } else {
            throw new ConfiguracionException("Valor incorrecto en uno de los valores de " + key);
        }
    }

    /**
     *
     */
    private void compruebaValores() throws ConfiguracionException {

        boolean tengoC = false;
        boolean tengoR = false;

        if (C1 != null && C2 != null) {
            tengoC = true;
        }
        if (R1 != null && R2 != null) {
            tengoR = true;
        }
        if (!tengoC && !tengoR) {
            throw new ConfiguracionException("No se han establecido los interfaces en ninguna de las Lineas a comprobar C1-C2 o R1-R2; se necesita al menos uno de los 2 pares para realizar la comprobacion");
        }

        boolean puedoEscucharC = false;
        boolean puedoEscucharR = false;

        if (tengoC && !gruposC.isEmpty()) {
            puedoEscucharC = true;
        }

        if (tengoR && !gruposR.isEmpty()) {
            puedoEscucharR = true;
        }

        if (!puedoEscucharC && !puedoEscucharR) {
            throw new ConfiguracionException("No se ha descrito ningun grupo multicast C1-C2 o R1-R2 y puerto para comprobar");
        }

    }

    public String getTitulo(String valor) {
        String iface1;
        String ip1;
        String iface2;
        String ip2;

        if (valor.equals("C")) {
            iface1 = C1;
            ip1 = iAddrC1 != null ? iAddrC1.getHostAddress() : "";

            iface2 = C2;
            ip2 = iAddrC2 != null ? iAddrC2.getHostAddress() : "";
        } else {
            iface1 = R1;
            ip1 = iAddrR1 != null ? iAddrR1.getHostAddress() : "";

            iface2 = R2;
            ip2 = iAddrR2 != null ? iAddrR2.getHostAddress() : "";
        }


        return "  LAN 1=" + iface1 + " (" + ip1 + ")   LAN 2=" + iface2 + " (" + ip2 + ")  ";
    }

    /**
     * @return the tiempoRefrescoColaSecs
     */
    public int getTiempoRefrescoColaSecs() {
        if (tiempoRefrescoColaSecs == -1) {
            return 5;
        } else {
            return tiempoRefrescoColaSecs;
        }
    }

    /**
     * @return the difTiempoSecuenciasMills
     */
    public long getDifTiempoSecuenciasMills() {
        if (difTiempoSecuenciasMills == -1) {
            return 700;
        } else {
            return difTiempoSecuenciasMills;
        }
    }

    private static String dameGrupoCorto(String grupo) {
        Matcher m = ipPattern.matcher(grupo);
        String grupoFormado = null;
        if (m.matches()) {
            grupoFormado = m.group(1) + m.group(2);
        }
        return grupoFormado;
    }
}
