/**
 * *****************************************************************************
 * Name : es.indra.scm.sacta.config.NetworkDomainConfigContext
 *
 * Project : [SC|SGU|PSI|...] Subproject: spv-alm CSCI : CSC :
 * ****************************************************************************** Last change info.
 * ****************************************************************************** #$Author: agbarajas $ #$Date: 2011-11-29 17:02:54 +0100
 * (mar, 29 nov 2011) $ #$Revision: 55258 $ ******************************************************************************
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import es.indra.scm.sca.sacta.compruebamulticast.utils.StringUtils;
import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class
 * <code>NetworkDomainConfigContext</code>.
 */
public class NetworkDomainConfigContext {

    private static final Logger log = Logger.getLogger(NetworkDomainConfigContext.class.getName());
    /**
     * Map that contains a collection of domains.
     */
    private HashMap<String, IDomain> mapDomains = new HashMap<String, IDomain>();
    /**
     * Map that contains a collection of centros.
     */
    private HashMap<String, ICentre> mapCentres = new HashMap<String, ICentre>();
    /**
     * Map that contains a collection of nodes.
     */
    private HashMap<String, INode> mapNodes = new HashMap<String, INode>();
    /**
     * Map that contains all interface.
     */
    private HashSet<InetAddress> netWorkInterface;
    /**
     * Map that contains sacta interface.
     */
    private HashSet<InetAddress> sactaNetWorkInterface;

    /**
     * @param properties IProperties
     */
//    public NetworkDomainConfigContext(IProperties properties) {
//
//        String resource = properties.getProperty(
//                SACTAContextConstants.NETWORK_DOMAIN_CONFIG_RESOURCE_CTX_PROPERTY);
//
//        init(resource);
//    }

    /**
     * @param resource String
     */
    public NetworkDomainConfigContext(String resource) throws FileNotFoundException {
        init(resource);
    }

    /**
     * Initialize interface.
     */
    private synchronized void initIface() {
        if (netWorkInterface == null || sactaNetWorkInterface == null) {
            try {
                netWorkInterface = new HashSet<InetAddress>();
                sactaNetWorkInterface = new HashSet<InetAddress>();
                ICentre[] centre = new ICentre[2];
                IDomain domain = getDomain(SACTAEnvironment.getInstance().getMyDomainId());
                if (domain != null) {
                    centre[0] = domain.getCentre(SACTAEnvironment.getInstance().getMyCentreId());
                    centre[1] = domain.getCentre(SACTAEnvironment.getInstance().getMyCentreIdComplementary());
                }

                for (Enumeration<NetworkInterface> eIface = NetworkInterface.getNetworkInterfaces(); eIface.hasMoreElements();) {
                    NetworkInterface iface = eIface.nextElement();
                    Enumeration<InetAddress> eAddr = iface.getInetAddresses();
                    while (eAddr.hasMoreElements()) {
                        InetAddress addr = eAddr.nextElement();
                        netWorkInterface.add(addr);
                        if (NetworkDomainUtil.isSactaInterface(addr, centre)) {
                            sactaNetWorkInterface.add(addr);
                        }
                    }
                }
            } catch (Exception e) {
                log.log(Level.SEVERE, "", e);
            }
        }
    }

    /**
     * @param resource String
     */
    private void init(String resource) throws FileNotFoundException {

        if (StringUtils.isEmpty(resource)) {
            log.log(Level.INFO,
                    "createNetworkDomainConfigContext - There is not a configuration resource defined");
        } else {
            LegacyNetworkDomainConfigReader reader = new LegacyNetworkDomainConfigReader();
//            try {
                reader.parse(this, resource);

//            } catch (FileNotFoundException e) {
//               
//                log.log(Level.SEVERE,
//                        "createNetworkDomainConfigContext - cargando [" + resource + "]", e);
//            }
        }
    }

    /**
     * return all network interface.
     *
     * @return HashSet<InetAddress>
     */
    public HashSet<InetAddress> getNetWorkInterface() {
        if (netWorkInterface == null) {
            initIface();
        }
        return netWorkInterface;
    }

    /**
     * return sacta network interface.
     *
     * @return HashSet<InetAddress>
     */
    public HashSet<InetAddress> getSactaNetWorkInterface() {
        if (sactaNetWorkInterface == null) {
            initIface();
        }
        return sactaNetWorkInterface;
    }

    /**
     * Add a domain to the collection of domains.
     *
     * @param domain Domain.
     */
    public void addDomain(IDomain domain) {
        mapDomains.put(domain.getName().toLowerCase(), domain);
    }

    /**
     * Add a centre to the collection of centres.
     *
     * @param centre Centre
     */
    public void addCentre(ICentre centre) {
        mapCentres.put(centre.getName().toLowerCase(), centre);
    }

    /**
     * Add a node to the collection of nodes.
     *
     * @param node Node
     */
    public void addNode(INode node) {
        mapNodes.put(node.getId().toLowerCase(), node);
    }

    /**
     * Get a domain from the list of domains through an identifier.
     *
     * @param id Id
     * @return
     * <code>IDomain</code> Domain
     */
    public IDomain getDomain(String id) {
        return mapDomains.get(id.toLowerCase());
    }

    /**
     * Get a centre from the list of centres through an identifier.
     *
     * @param id Id
     * @return
     * <code>ICentre</code> Centre
     */
    public ICentre getCentre(String id) {
        return mapCentres.get(id.toLowerCase());
    }

    /**
     * Get a node from the list of nodes through an identifier.
     *
     * @param id Id
     * @return
     * <code>INode</code> node
     */
    public INode getNode(String id) {

        String nodo = id.toLowerCase();
        //System.out.println("Nodo1: " + nodo);
        INode ret = mapNodes.get(nodo);

        if (ret == null) {
            // napa DMO... The id defined in the ETIQUETA.TXT is not the same that it is configured
            // in the com_ip_centros, so we try again with this transformation...
            nodo = SACTAAgentNamingUtils.convertLabelIdToComIPCentrosId(id.toLowerCase());
            //System.out.println("Nodo2: " + nodo);
            ret = mapNodes.get(nodo);
        }

        return ret;
    }

    /**
     * Gets a domain through the first octet of the ip address.
     *
     * @param ipPart IP Part.
     * @return
     * <code>IDomain</code> domain
     */
    public IDomain getDomainByIPPart(String ipPart) {
        // TODO dmo... hacerlo decentemente....

        IDomain ret = null;

        if (ipPart != null) {
            for (IDomain domain : mapDomains.values()) {
                if (ipPart.equalsIgnoreCase(domain.getIPPart())) {
                    ret = domain;
                    break;
                }
            }
        }

        return ret;
    }

    /**
     * Gets a centre through the first octet of the ip address.
     *
     * @param ipPart IP Part.
     * @return
     * <code>ICentre</code> centre
     */
    public ICentre getCentreByIPPart(String ipPart) {
        // TODO dmo... hacerlo decentemente....
        // Se espara xx.xx.xx

        ICentre centre = null;

        String[] parts = ipPart.split("\\.");

        if (parts != null && parts.length == 3) {
            IDomain domain = getDomainByIPPart(parts[0]);

            if (domain != null) {
                centre = domain.getCentreByIPPart(parts[1] + "." + parts[2]);
            }
        }

        return centre;
    }

    /**
     * Gets a node through the first octet of the ip address.
     *
     * @param ipPart IP Part.
     * @return
     * <code>INode</code> node
     */
    public INode getNodeByIPPart(String ipPart) {
        // TODO dmo... hacerlo decentemente....
        // Se espera xx

        INode ret = null;

        if (ipPart != null) {
            for (INode node : mapNodes.values()) {
                if (ipPart.equals(node.getIPPart())) {
                    ret = node;

                    break;
                }
            }
        }

        return ret;
    }
}
