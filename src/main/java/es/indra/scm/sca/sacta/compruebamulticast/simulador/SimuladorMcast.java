/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.simulador;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aferrero
 */
public class SimuladorMcast implements Runnable {

    MulticastSocket mcastSocket1;
    MulticastSocket mcastSocket2;

    public static void main(String[] args) throws IOException {
        Properties props = System.getProperties();
        props.setProperty("java.net.preferIPv4Stack", "true");
        System.setProperties(props);

        Thread t = new Thread(new SimuladorMcast());
        t.start();
    }

    public SimuladorMcast() {
    }

    
    public void run() {
        try {
            int port = 21460;

            ////
            String interfazLan1 = "en0";
            String addressLAN1 = "235.10.108.12";


            InetAddress iAddr_mcast_group1 = InetAddress.getByName(addressLAN1);
            InetSocketAddress groupSockAddr1 = new InetSocketAddress(iAddr_mcast_group1, port);

            NetworkInterface networkInterface1 = NetworkInterface.getByName(interfazLan1);


            mcastSocket1 = new MulticastSocket(port);
            mcastSocket1.setNetworkInterface(networkInterface1);
            mcastSocket1.setSoTimeout(32);
            mcastSocket1.joinGroup(groupSockAddr1, networkInterface1);

            ////

            String interfazLan2 = "en1";
            String addressLAN2 = "235.110.108.12";



            InetAddress iAddr_mcast_group2 = InetAddress.getByName(addressLAN2);
            InetSocketAddress groupSockAddr2 = new InetSocketAddress(iAddr_mcast_group2, port);

            NetworkInterface networkInterface2 = NetworkInterface.getByName(interfazLan2);


            mcastSocket2 = new MulticastSocket(port);
            mcastSocket2.setNetworkInterface(networkInterface2);
            mcastSocket2.setSoTimeout(32);
            mcastSocket2.joinGroup(groupSockAddr2, networkInterface2);

            String bb = "00000000";
            byte ss = (byte) Short.parseShort(bb, 2);
            byte[] buffer = new byte[]{ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss, ss};

            int veces = 100;
            int secuencia = 0;
            while (veces > 0) {
//                System.out.println("Secuencia: " + secuencia);

                byte ssec = (byte) Short.parseShort("" + secuencia, 10);

                System.out.println("Secuencia: " + secuencia + " (" + Integer.toBinaryString(ssec & 0xFF) + ")");

                buffer[13] = ssec;

                //if (secuencia % 2 != 0) {
                DatagramPacket packet1 = new DatagramPacket(buffer, buffer.length, groupSockAddr1);
                mcastSocket1.send(packet1);
                //} else {
                //    System.out.println("NO Envio en LAN1");
                //}

                if (secuencia % 10 != 0) {
                    DatagramPacket packet2 = new DatagramPacket(buffer, buffer.length, groupSockAddr2);
                    mcastSocket2.send(packet2);
                } else {
                    System.out.println("NO Envio en LAN2");

//                    System.out.println("Envio 2 seguidos a LAN2");
//                    DatagramPacket packet2 = new DatagramPacket(buffer, buffer.length, groupSockAddr2);
//                    mcastSocket2.send(packet2);
//                    Thread.sleep(100);
//                    mcastSocket2.send(packet2);
                }


                Thread.sleep(500);




                if (secuencia >= 254) {
                    secuencia = 0;
                } else {
                    secuencia++;
                }
                veces--;
            }

        } catch (IOException e) {
            System.err.println(e);
        } catch (InterruptedException ex) {
            Logger.getLogger(SimuladorMcast.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
