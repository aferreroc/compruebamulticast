/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author aferrero
 */
public class JTableButtonRenderer implements TableCellRenderer {		
  @Override public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    JButton button = (JButton)value;
//    if (isSelected) {
//      button.setForeground(table.getSelectionForeground());
//      button.setBackground(table.getSelectionBackground());
//    } else {
//      button.setForeground(table.getForeground());
//      button.setBackground(UIManager.getColor("Button.background"));
//    }
    return button;	
  }
}
