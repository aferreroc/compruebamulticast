/**
 * *****************************************************************************
 * Name : es.indra.scm.sacta.config.INode Project : [SC|SGU|PSI|...] Subproject: spv-alm CSCI : CSC :
 * ****************************************************************************** Last change info.
 * ****************************************************************************** #$Author: dmoya $ #$Date: 2011-09-16 11:59:51 +0200 (vie,
 * 16 sep 2011) $ #$Revision: 51341 $ ******************************************************************************
 */
package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.util.List;

/**
 * Node inteface extends
 * <code>IXMLObjectWriter</code>.
 *
 * Class that contains methods for managing a node.
 */
public interface INode extends IIdentificableObject, IXMLObjectWriter {

    /**
     * Assign the IP part of the node.
     *
     * @param value Value
     */
    void setIPPart(String value);

    /**
     * Gets the first octet of the IP address of the node.
     *
     * @return IPPart
     */
    String getIPPart();

    /**
     * Gets the ip address through the center of the domain.
     *
     * @param domain Domain
     * @param centre Centre
     * @return list of ip
     */
    List<String> getIPs(String domain, String centre);

    /**
     * Gets the ip address through the
     * <code>ICenter</code>.
     *
     * @param centre Centre
     * @return list of ip (multicast/unicast).
     */
    List<String> getIPs(ICentre centre);

    // void setPort(int port);
    /**
     * Add a port to the network configuration.
     *
     * @param protocolId Protocol ID
     * @param port Port
     */
    void addPort(String protocolId, int port);

    // int getPort();
    /**
     * Gets a port to the network configuration.
     *
     * @param protocolId Protocol ID.
     * @return Port.
     */
    int getPort(String protocolId);

    /**
     * Add a node to a child.
     *
     * @param node Node.
     */
    void addChild(INode node);

    /**
     * Returns a list of nodes of a child.
     *
     * @return a list of nodes of a child
     */
    List<INode> getChildren();

    /**
     * Add a node to a parent.
     *
     * @param node Node
     */
    void addParent(INode node);

    /**
     * Returns a list of nodes of a parents.
     *
     * @return a list of nodes of a parents
     */
    List<INode> getParents();

    /**
     * Add a static IP address to node.
     *
     * @param staticIP Static IP
     */
    void addStaticIP(String staticIP, int lan);

    /**
     * Verify that a child is in a group.
     *
     * @return true if this group
     */
    boolean isGroup();

    List<String> getMulticastStaticIP(int lan);
}
