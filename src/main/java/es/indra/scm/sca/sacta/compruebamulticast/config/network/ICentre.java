/*******************************************************************************
 * Name      : es.indra.scm.sacta.config.ICentre
 * Project   : [SC|SGU|PSI|...]
 * Subproject: spv-alm
 * CSCI      :
 * CSC       :
 *******************************************************************************
 * Last change info.
 *******************************************************************************
 * #$Author: dmoya $
 * #$Date: 2011-09-16 11:59:51 +0200 (vie, 16 sep 2011) $
 * #$Revision: 51341 $
 *******************************************************************************
 */

package es.indra.scm.sca.sacta.compruebamulticast.config.network;

import java.util.List;



/**
 * Centre interface extends <code>IXMLObjectWriter</code>.
 * It defines the methods for the center.
 */
public interface ICentre extends IIdentificableObject, IXMLObjectWriter {

	/**
	 * Assigned a identifier of center.
	 * @param id String ID.
	 */
	void setId(String id);

	/**
	 * Returns the Name of the center.
	 * @return String ID.
	 */
	String getName();

	/**
	 * Assigned a Name of center.
	 * @param name String Name.
	 */
	void setName(String name);

	/**
	 * Returns the Network Mask of the center.
	 * @return String Network Mask.
	 */
	String getNetworkMask();

	/**
	 * Returns the gateway of the center.
	 * @return String Gateway.
	 */
	String getGateway();

	/**
	 * Returns the X25Link of the center.
	 * @return String X25Link.
	 */
	String getX25Link();



	/**
	 * Returns the domain of the center.
	 * @return <code>IDomain</code> Domain.
	 */
	IDomain getDomain();

	// List<String> getUnicastIPParts();

	// List<String> getMulticastIPParts();

	/**
	 * Return IP Part list.
	 * @return List of IP.
	 */
	List<IPPartsPair> getIPPartsPairs();

	/**
	 * Check if the IP address of the center is unicast.
	 * @param ipPart IP Part.
	 * @return true if unicast
	 */
	boolean isUnicastIPPart(String ipPart);

}
