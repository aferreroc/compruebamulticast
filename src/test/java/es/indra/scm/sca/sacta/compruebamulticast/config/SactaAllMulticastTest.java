/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast.config;

import es.indra.scm.sca.sacta.compruebamulticast.ParGrupoMulticast;
import es.indra.scm.sca.sacta.compruebamulticast.config.network.MulticastServerConfig;
import es.indra.scm.sca.sacta.compruebamulticast.config.network.NetworkDomainUtil;
import es.indra.scm.sca.sacta.compruebamulticast.config.network.SACTAEnvironment;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 * @author aferrero
 */
public class SactaAllMulticastTest extends TestCase {

    public SactaAllMulticastTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    // TODO add test methods here. The name must begin with 'test'. For example:

    public void testSactaAllMulticast() {
        try {

            String[] centros = {"ACC_LECM", "ACC_LECS", "ACC_LECB", "ACC_GCCC", "TMA_LEPA"};
            
            
            System.out.println("==================================================");

            for (String centro : centros) {
                //C
                Map<String, ParGrupoMulticast> gruposC =
                        SactaAllMulticast.dameSactaAllMulticast(centro, "/Users/aferrero/Developer/proyectos/INDRA/CompruebaMulticast/src/main/resources/ficheros_sacta/COM_IP_CENTROS.CFG");

                //R
                String centroR = centro + "_R";
                Map<String, ParGrupoMulticast> gruposR =
                        SactaAllMulticast.dameSactaAllMulticast(centroR, "/Users/aferrero/Developer/proyectos/INDRA/CompruebaMulticast/src/main/resources/ficheros_sacta/COM_IP_CENTROS.CFG_radar");


                System.out.println(centro + " -> Totales CONTROL: " + gruposC.size() + " RADAR: " + gruposR.size());
            }

        } catch (Exception ex) {
            Logger.getLogger(SactaAllMulticastTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//    public void testSactaEnvironment() {
//        SACTAEnvironment.getInstance(
//                "/Users/aferrero/Developer/proyectos/INDRA/CompruebaMulticast/src/main/resources/ficheros_sacta");
//        SACTAEnvironment.doTest();
//    }
}
