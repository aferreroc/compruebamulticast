/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.indra.scm.sca.sacta.compruebamulticast;

import es.indra.scm.sca.sacta.compruebamulticast.config.Configuracion;
import java.io.FileInputStream;
import java.net.*;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aferrero
 */
public class MulticastSenderRunnable implements Runnable {

    //private static final Logger log = Logger.getLogger(MulticastSenderRunnable.class.getName());
    long espera;
    int numReinicioSecuencia;
    int numTotalSecuenciasEnviar;

    private MulticastSenderRunnable() {
    }

    public MulticastSenderRunnable(long espera, int numReinicioSecuencia, int numTotalSecuenciasEnviar) {

        this.espera = espera;
        if (numReinicioSecuencia > 279 || numReinicioSecuencia < 0) {
            this.numReinicioSecuencia = 279;
        } else {
            this.numReinicioSecuencia = numReinicioSecuencia;
        }

        if (numTotalSecuenciasEnviar <= 0) {
            this.numTotalSecuenciasEnviar = Integer.MAX_VALUE;
        } else {
            this.numTotalSecuenciasEnviar = numTotalSecuenciasEnviar;
        }

    }

    public void run() {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("target/classes/CompruebaMulticast_dev.conf"));
            Configuracion conf = new Configuracion(properties, false, true);

            //varios tests
            System.out.println(conf.C1 + " " + conf.C2 + " " + conf.R1 + " " + conf.R2);

            Enumeration<NetworkInterface> ifs = NetworkInterface.getNetworkInterfaces();

            while (ifs.hasMoreElements()) {
                System.out.println(ifs.nextElement().getDisplayName());
            }
            //fin varios tests

            int secuencia = 0;
            int numSecuencias = 0;
            byte[] outbuf = new byte[20];
            for (int i = 0; i < outbuf.length; i++) {
                outbuf[i] = 0;
            }

            //while (true) {
            while (numSecuencias < numTotalSecuenciasEnviar) {

                numSecuencias++;

                //una cabecera sacta tiene 160 bits == 20 bytes
                //la secuencia empieza en el bit 100 y mide 12 bits
                //por lo que nos interesan los bytes 13 y 14 (empezando en 0 son los bytes 12 y 13)
                outbuf[12] = (byte) ((secuencia >>> 8) & 0xFF);
                outbuf[13] = (byte) ((secuencia >>> 0) & 0xFF);


                //System.out.println("Secuencia a enviar: " + secuencia + " convertida: " + MulticastGroupListenerRunnable.dameNumeroSecuencia(outbuf, outbuf.length));

                for (ParGrupoMulticast parGrMcast : conf.gruposC.values()) {
                    sendPacket(outbuf, parGrMcast.GR1, parGrMcast.puerto);

                    if ((secuencia % 2) == 0) {
                        sendPacket(outbuf, parGrMcast.GR2, parGrMcast.puerto);
                        System.out.println("Envio secuencia Lan2: " + secuencia + " num: " + numSecuencias);
                    }

                    //sendPacket(outbuf, parGrMcast.GR2, parGrMcast.puerto);
                    Thread.sleep(espera);
                }

                for (ParGrupoMulticast parGrMcast : conf.gruposR.values()) {
                    sendPacket(outbuf, parGrMcast.GR1, parGrMcast.puerto);
                    sendPacket(outbuf, parGrMcast.GR2, parGrMcast.puerto);
                    Thread.sleep(espera);
                }
                if (secuencia >= numReinicioSecuencia) {
                    secuencia = 0;
                } else {
                    secuencia++;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendPacket(byte[] outbuf, String mcast_group, int mcast_port) throws Exception {

        DatagramSocket mcastSocket = new DatagramSocket();
        DatagramPacket packet = new DatagramPacket(outbuf, outbuf.length, InetAddress.getByName(mcast_group), mcast_port);
        mcastSocket.send(packet);

    }
}
