import socket, struct, time

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)

secuencia = 0

for i in range(100):
    data = struct.pack('!HHHHHHHHHH', 0,0,0,0,0,0,i,0,0,0)
    sock.sendto(data, ("228.40.102.1", 17000))
    if secuencia % 2:
        sock.sendto(data, ("228.140.102.1", 17000))
    time.sleep(0.1)

    sock.sendto(data, ("229.40.104.48", 21411))
    if secuencia % 2:
        sock.sendto(data, ("229.140.104.48", 21411))
    time.sleep(0.1)

    #228.40.104.48;228.140.104.48;21411
    sock.sendto(data, ("228.40.104.48", 21411))
    sock.sendto(data, ("228.140.104.48", 21411))
    time.sleep(0.1)

    #235.10.104.48;235.110.104.48;21411
    sock.sendto(data, ("235.10.104.48", 21411))
    sock.sendto(data, ("235.110.104.48", 21411))
    time.sleep(0.1)

    #235.10.106.48;235.110.106.48;21411
    sock.sendto(data, ("235.10.106.48", 21411))
    sock.sendto(data, ("235.110.106.48", 21411))
    time.sleep(0.1)

    if (secuencia==287):
        secuencia=-1

    secuencia+=1


